<?php
namespace BooklyCustomFields\Lib;
// ADWAVE START
/**
 * Class DateRestriction
 * @package BooklyCustomFields\Lib
 */
class DateRestriction
{
    public static function is_date_restricted($custom_field)
    {
        $is_date_restricted = false;

        if (property_exists($custom_field, 'items') && $custom_field->items) {
            foreach ( $custom_field->items as $item )
            {
                if (self::is_date_obj($item['label']))
                {
                    $is_date_restricted = true;
                }
            }
        }
        return $is_date_restricted;
    }

    public static function is_booked_date($custom_field, $booked_dates)
    {
        $is_booked_date = false;
        if ($custom_field->items) {
            foreach ( $custom_field->items as $item )
            {
                if (self::is_date_obj($item['label'])) {
                    $date = json_decode($item['label'], true);
                    $from_date = strtotime($date['data'][0]);
                    $to_date = strtotime($date['data'][1]);

                    if ($to_date < $from_date) {
                        $tmp = $from_date;
                        $from_date = $to_date;
                        $to_date = $tmp;
                    }

                    foreach ($booked_dates as $booked_date) {
                        $booked_date_time = strtotime($booked_date);
                        if ($booked_date_time >= $from_date && $booked_date_time <= $to_date) {
                            $is_booked_date = true;
                        }
                    }
                }
            }
        }
        return $is_booked_date;
    }

    public static function is_date($x) {
        return (date('Y-m-d', strtotime($x)) == $x);
    }

    public static function is_date_obj($x) {
        $json_obj = json_decode($x, true);
        $is_json =  (json_last_error() == JSON_ERROR_NONE);
        if ($is_json && is_array($json_obj) && $json_obj['type'] == 'date') {
            return true;
        }
        return false;
    }

    public static function get_non_date_fields ($custom_field)
    {
        $result = array();
        if ($custom_field->items) {
            foreach ( $custom_field->items as $item )
            {
                if (!self::is_date_obj($item['label']))
                {
                    $result[] = $item;
                }
            }
        }
        return $result;
    }

    public static function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}
// ADWAVE END