<?php
namespace BooklyCustomFields\Backend\Modules\CustomFields;

use Bookly\Lib as BooklyLib;

function count_date ($list) {
    $count = count($list);
    $empty_count = 0;
    foreach ($list as $key => $value) {
        if (strlen($value) == 0) {
            $empty_count++;
        }
    }
    return $count - $empty_count;
}

/**
 * Class Ajax
 * @package BooklyCustomFields\Backend\Modules\CustomFields
 */
class Ajax extends BooklyLib\Base\Ajax
{
    /**
     * Save custom fields.
     */
    public static function saveCustomFields()
    {
        $fields = self::parameter( 'fields', '[]' );
        $per_service     = (int) self::parameter( 'per_service' );
        $merge_repeating = (int) self::parameter( 'merge_repeating' );
        $custom_fields   = json_decode( $fields, true );

        foreach ( $custom_fields as $custom_field ) {
            switch ( $custom_field['type'] ) {
                case 'textarea':
                // <!-- ADWAVE START -->
                case 'date':
                // <!-- ADWAVE END -->
                case 'text-content':
                case 'text-field':
                case 'captcha':
                case 'file':
                    do_action(
                        'wpml_register_single_string',
                        'bookly',
                        sprintf(
                            'custom_field_%d_%s',
                            $custom_field['id'],
                            sanitize_title( $custom_field['label'] )
                        ),
                        $custom_field['label']
                    );
                    break;
                case 'checkboxes':
                case 'radio-buttons':
                case 'drop-down':
                    do_action(
                        'wpml_register_single_string',
                        'bookly',
                        sprintf(
                            'custom_field_%d_%s',
                            $custom_field['id'],
                            sanitize_title( $custom_field['label'] )
                        ),
                        $custom_field['label']
                    );
                    foreach ( $custom_field['items'] as $label ) {
                        do_action(
                            'wpml_register_single_string',
                            'bookly',
                            sprintf(
                                'custom_field_%d_%s=%s',
                                $custom_field['id'],
                                sanitize_title( $custom_field['label'] ),
                                sanitize_title( $label )
                            ),
                            $label
                        );
                    }
                    break;
            }
        }

        foreach ($custom_fields as $key => $value) {
            if ($value['type'] == 'checkboxes') {
                $items = $value['items'];
                $clean_item_list = array();
                foreach ($items as $key_value => $item_value) {
                    if (is_array($item_value) && !is_string($item_value)) {
                        if ($item_value['type'] == 'date') {
                            if (count_date($item_value['data']) > 1) {
                                $clean_item_list[] = json_encode($item_value);
                            }
                        } else {
                            $clean_item_list[] = $item_value;
                        }
                    } else {
                        $clean_item_list[] = $item_value;
                    }
                }
                $custom_fields[$key]['items'] = $clean_item_list;
            }
        }
        $fields = json_encode($custom_fields);
        // BooklyLib\Proxy\Files::saveCustomFields( $custom_fields );

        update_option( 'bookly_custom_fields_data', $fields );
        update_option( 'bookly_custom_fields_per_service', $per_service );
        update_option( 'bookly_custom_fields_merge_repeating', $merge_repeating );
        wp_send_json_success();
    }

    public static function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}