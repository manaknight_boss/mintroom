<?php
namespace BooklyPro\Lib\Notifications\Assets\Combined;

use Bookly\Lib as BooklyLib;
use Bookly\Lib\Config;
use Bookly\Lib\DataHolders\Booking\Order;
use Bookly\Lib\Notifications\Assets\Order\Codes as OrderCodes;
use Bookly\Lib\Proxy;
use Bookly\Lib\Utils;

/**
 * Class Codes
 * @package BooklyPro\Lib\Notifications\Assets\Combined
 */
class Codes extends OrderCodes
{
    public $cart_info;

    /**
     * @inheritdoc
     */
    public function __construct( Order $order )
    {
        parent::__construct( $order );

        $this->cart_info = array();

        $total = 0.0;

        $first = current( $order->getItems() );
        $this->client_timezone = $first->getCA()->getTimeZone() ?: (
        $first->getCA()->getTimeZoneOffset() !== null
            ? 'UTC' . Utils\DateTime::guessTimeZone( - $first->getCA()->getTimeZoneOffset() * 60 )
            : ''
        );

        foreach ( $order->getItems() as $item ) {
            $sub_items = array();
            if ( $item->isSeries() ) {
                $sub_items = $item->getItems();
                if ( get_option( 'bookly_recurring_appointments_payment' ) == 'first' ) {
                    array_splice( $sub_items, 1 );
                }
            } else {
                $sub_items[] = $item;
            }
            foreach ( $sub_items as $sub_item ) {
                // Sub-item price.
                $price = $sub_item->getTotalPrice();

                $deposit_price = Config::depositPaymentsActive()
                    ? BooklyLib\Proxy\DepositPayments::prepareAmount( $price, $sub_item->getDeposit(), $sub_item->getCA()->getNumberOfPersons() )
                    : 0;

                // Prepare data for {cart_info} || {cart_info_c}.
                //ADWAVE START
                $subitem_duration_hours = ($sub_item->getService()->getDuration() / 3600 * $sub_item->getCA()->getUnits());
                //ADWAVE END
                $this->cart_info[] = array(
                    'appointment_start' => $this->applyItemTz( $sub_item->getAppointment()->getStartDate(), $sub_item ),
                    'cancel_url'        => admin_url( 'admin-ajax.php?action=bookly_cancel_appointment&token=' . $sub_item->getCA()->getToken() ),
                    'service_name'      => $sub_item->getService()->getTranslatedTitle(),
                    //ADWAVE START
                    'id' => $sub_item->getAppointment()->getId(),
                    'appointment_price' => $this->getServicePriceDateAdjusted($sub_item->getService(), $sub_item->getAppointment()->getStartDate(),$sub_item->getCA()->getUnits(), $price),
                    'duration'          => $subitem_duration_hours > 1 ? $subitem_duration_hours.' hours' : $subitem_duration_hours.' hour',
                    //ADWAVE END
                    'staff_name'        => $sub_item->getStaff()->getTranslatedName(),
                    'extras'            => (array) BooklyLib\Proxy\ServiceExtras::getInfo( $sub_item->getExtras(), true ),
                    'tax'               => Config::taxesActive() ? $sub_item->getTax() : null,
                    'deposit'           => Config::depositPaymentsActive()
                        ? BooklyLib\Proxy\DepositPayments::formatDeposit( $deposit_price, $sub_item->getDeposit() )
                        : null,
                    'appointment_start_info' => $sub_item->getService()->getDuration() < DAY_IN_SECONDS
                        ? null :
                        $sub_item->getService()->getStartTimeInfo(),
                );

                // Total price.
                $total += $price;
            }
        }

        if ( ! $order->hasPayment() ) {
            $this->total_price = $total;
        }
    }

    //ADWAVE START
    private function in_date_range($booking_date_start, $specific_date_date)
    {
        $found = false;
        $index = -1;
        $book_date = strtotime($booking_date_start);
        foreach ($specific_date_date as $key => $value) {
            $from = strtotime($value['from']);
            $to = strtotime($value['to']);
            if ($book_date >= $from && $book_date <= $to) {
                $found = true;
                $index = $key;
            }
        }

        return array(
            'exist' => $found,
            'index' => $index
        );
    }

    public function getServicePriceDateAdjusted ($service, $startDate, $units, $oldServicePrice) {
        $specific_date_price  = json_decode($service->getPriceSpecificDatePrice(), true);
        $specific_date_date  = json_decode($service->getPriceSpecificDateDate(), true);
        $booking_date_start = date( 'Y-m-d', strtotime( $startDate ) );
        $date_range = $this->in_date_range($booking_date_start, $specific_date_date);
        if ($date_range['exist']) {
            $service_price = ((int)$specific_date_price[$date_range['index']]) * $units;
        } else {
            $service_price = $oldServicePrice;
        }

        $day_of_week = (int)date('N', strtotime( $startDate ));
        $is_week_day = ($day_of_week < 6) && ($day_of_week > 0);

        $is_competitive_day = false;
        $bookly_competitive_days = get_option('bookly_competitive_days', array());
        if (!$bookly_competitive_days) {
            $bookly_competitive_days = array();
        } else {
            $bookly_competitive_days = json_decode($bookly_competitive_days, true);
        }
        $book_date = strtotime($startDate);
        foreach ($bookly_competitive_days as $key => $date_range_obj) {
            $from = strtotime($date_range_obj['from']);
            $to = strtotime($date_range_obj['to']);
            if ($book_date >= $from && $book_date <= $to) {
                $is_competitive_day = true;
            }
        }
        if ($units >= 8 && $is_week_day && !$is_competitive_day) {
            $service_price = $service_price * 0.7;
        } else if ($units >= 4 && $is_week_day && !$is_competitive_day) {
            $service_price = $service_price * 0.8;
        } else if ($units >= 2 && $is_week_day && !$is_competitive_day) {
            $service_price = $service_price * 0.9;
        }
        return $service_price;
    }
    //ADWAVE END

    /**
     * @inheritdoc
     */
    protected function getReplaceCodes( $format )
    {
        $replace_codes = parent::getReplaceCodes( $format );

        $cart_info_c = $cart_info = '';

        // Cart info.
        $cart_info_data = $this->cart_info;
        if ( ! empty ( $cart_info_data ) ) {
            $cart_columns = get_option( 'bookly_cart_show_columns', array() );
            if ( empty( $cart_columns ) ) {
                $cart_columns = array(
                    'id'  => array( 'show' => '1', ),
                    'service'  => array( 'show' => '1', ),
                    'date'     => array( 'show' => '1', ),
                    'time'     => array( 'show' => '1', ),
                    'employee' => array( 'show' => '1', ),
                    'price'    => array( 'show' => '1', ),
                    'deposit'  => array( 'show' => (int) Config::depositPaymentsActive() ),
                    'tax'      => array( 'show' => (int) Config::taxesActive(), ),
                );
            }
            //ADWAVE START
            $id_array = array(
                'id' => array( 'show' => '1', )
            );
            $cart_columns = $id_array + $cart_columns;
            $cart_columns['duration'] = array( 'show' => '1', );
            //ADWAVE END
            if ( ! Proxy\Taxes::showTaxColumn() ) {
                unset( $cart_columns['tax'] );
            }
            if ( ! Config::depositPaymentsActive() ) {
                unset( $cart_columns['deposit'] );
            }
            $ths = array();
            foreach ( $cart_columns as $column => $attr ) {
                if ( $attr['show'] ) {
                    switch ( $column ) {
                        case 'service':
                            $ths[] = Utils\Common::getTranslatedOption( 'bookly_l10n_label_service' );
                            break;
                        case 'date':
                            $ths[] = __( 'Date', 'bookly' );
                            break;
                        case 'time':
                            $ths[] = __( 'Time', 'bookly' );
                            break;
                        case 'tax':
                            $ths[] = __( 'Tax', 'bookly' );
                        //ADWAVE START
                        case 'duration':
                            $ths[] = 'Duration';
                            break;
                        case 'id':
                            $ths[] = 'Booking #';
                            break;
                        //ADWAVE END
                        case 'employee':
                            $ths[] = Utils\Common::getTranslatedOption( 'bookly_l10n_label_employee' );
                            break;
                        case 'price':
                            $ths[] = __( 'Price', 'bookly' );
                            break;
                        case 'deposit':
                            $ths[] = __( 'Deposit', 'bookly' );
                            break;
                    }
                }
            }
            $trs = array();
            foreach ( $cart_info_data as $data ) {
                $tds = array();
                foreach ( $cart_columns as $column => $attr ) {
                    if ( $attr['show'] ) {
                        switch ( $column ) {
                            case 'service':
                                $service_name = $data['service_name'];
                                if ( ! empty ( $data['extras'] ) ) {
                                    $extras = '';
                                    if ( $format == 'html' ) {
                                        foreach ( $data['extras'] as $extra ) {
                                            $extras .= '<li>' . $extra['title'] . '</li>';
                                        }
                                        $extras = '<ul>' . $extras . '</ul>';
                                    } else {
                                        foreach ( $data['extras'] as $extra ) {
                                            $extras .= ', ' . str_replace( '&nbsp;&times;&nbsp;', ' x ', $extra['title'] );
                                        }
                                    }
                                    $service_name .= $extras;
                                }
                                $tds[] = $service_name;
                                break;
                            case 'date':
                                $tds[] = $data['appointment_start'] === null ? __( 'N/A', 'bookly' ) : Utils\DateTime::formatDate( $data['appointment_start'] );
                                break;
                            case 'time':
                                if ( $data['appointment_start_info'] !== null ) {
                                    $tds[] = $data['appointment_start_info'];
                                } else {
                                    $tds[] = $data['appointment_start'] === null ? __( 'N/A', 'bookly' ) :  Utils\DateTime::formatTime( $data['appointment_start'] );
                                }
                                break;
                            case 'tax':
                                $tds[] = Utils\Price::format( $data['tax'] );
                                break;
                            case 'employee':
                                $tds[] = $data['staff_name'];
                                break;
                            case 'price':
                                $tds[] = Utils\Price::format( $data['appointment_price'] );
                                break;
                            case 'deposit':
                                $tds[] = $data['deposit'];
                                break;
                            //ADWAVE START
                            case 'duration':
                                $tds[] = $data['duration'];
                                break;
                            case 'id':
                                $tds[] = 'MR_' . $data['id'];
                                break;
                            //ADWAVE END
                        }
                    }
                }
                $tds[] = $data['cancel_url'];
                $trs[] = $tds;
            }
            if ( $format == 'html' ) {
                $cart_info   = '<table cellspacing="1" border="1" cellpadding="5"><thead><tr><th>' . implode( '</th><th>', $ths ) . '</th></tr></thead><tbody>';
                $cart_info_c = '<table cellspacing="1" border="1" cellpadding="5"><thead><tr><th>' . implode( '</th><th>', $ths ) . '</th><th>' . __( 'Cancel', 'bookly' ) . '</th></tr></thead><tbody>';
                foreach ( $trs as $tr ) {
                    $cancel_url   = array_pop( $tr );
                    $cart_info   .= '<tr><td>' . implode( '</td><td>', $tr ) . '</td></tr>';
                    $cart_info_c .= '<tr><td>' . implode( '</td><td>', $tr ) . '</td><td><a href="' . $cancel_url . '">' . __( 'Cancel', 'bookly' ) . '</a></td></tr>';
                }
                $cart_info   .= '</tbody></table>';
                $cart_info_c .= '</tbody></table>';
            } else {
                foreach ( $trs as $tr ) {
                    $cancel_url = array_pop( $tr );
                    foreach ( $ths as $position => $column ) {
                        $cart_info   .= $column . ' ' . $tr[ $position ] . "\r\n";
                        $cart_info_c .= $column . ' ' . $tr[ $position ] . "\r\n";
                    }
                    $cart_info .= "\r\n";
                    $cart_info_c .= __( 'Cancel', 'bookly' )  . ' ' . $cancel_url . "\r\n\r\n";
                }
            }
        }

        // Replace codes.
        $replace_codes += array(
            '{cart_info}'   => $cart_info,
            '{cart_info_c}' => $cart_info_c,
        );

        return $replace_codes;
    }
}