<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
// ADWAVE START
use Bookly\Lib\Config;
use Bookly\Lib\Entities\Service;
use Bookly\Backend\Modules\Services\Proxy;
?>
<div class="col-sm-12 bookly-js-service bookly-js-service-simple bookly-js-service-collaborative bookly-js-service-compound bookly-js-service-package">
    <div class="form-group">
        <label for="price_specific_date_<?php echo $service['id'] ?>"><?php _e( 'Specific Date Prices', 'bookly' ) ?></label>
        <p class="help-block"><?php _e( 'Set the hourly rate for specific dates', 'bookly' ) ?></p>
        <br/>
        <button type="button" class="add_price_specific_date_price ladda-button btn btn-success" data-id="<?php echo $service['id'] ?>">
            <span class="ladda-label"><i class="glyphicon glyphicon-plus"></i> Add Specific Date Price</span>
        </button>
        <br/>
        <br/>
        <div class="add_price_specific_date_price_container">
            <?php if (strlen($service['price_specific_date_price']) > 3) {
                $price_list = json_decode($service['price_specific_date_price'], true);
                $date_list = json_decode($service['price_specific_date_date'], true);
                for ($i=0; $i < count($price_list); $i++) {
                    echo '<div class="row" style="margin:10px 0px">';
                    echo '<div class="col-xs-5">';
                    echo '<input id="price_specific_date_' . $service['id'] . '" class="form-control " min="1" step="1" name="price_specific_date_price[]" value="' . $price_list[$i]. '" type="number" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="New Unit Price"/>';
                    echo '</div>';
                    echo '<div class="col-xs-5">';
                    echo 'From<br/><input id="price_specific_date_from_date_' . $service['id'] . '" class="form-control"  name="price_specific_date_from_date[]" value="' . $date_list[$i]['from']. '" type="date" /><br/>';
                    echo 'To<br/><input id="price_specific_date_to_date_' . $service['id'] . '" class="form-control"  name="price_specific_date_to_date[]" value="' . $date_list[$i]['to']. '" type="date" />';
                    echo '</div>';
                    echo '<div class="col-xs-2">';
                    echo '<button type="button" class="add_price_specific_date_price_remove_button ladda-button btn btn-danger" data-id="' . $service['id'] . '">';
                    echo '<span class="ladda-label"><i class="glyphicon glyphicon-trash"></i> Remove</span>';
                    echo '</button>';
                    echo '</div>';
                    echo '</div>';
                }
            } else {
                ?>
                <div class="row" style="margin:10px 0px">
                    <div class="col-xs-5">
                        <input id="price_specific_date_<?php echo $service['id'] ?>" class="form-control " min="1" step="1" name="price_specific_date_price[]" value="" type="number" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="New Unit Price"/>
                    </div>
                    <div class="col-xs-5">
                        From<br/><input id="price_specific_date_from_date_<?php echo $service['id'] ?>" class="form-control"  name="price_specific_date_from_date[]" value="" type="date" /><br/>
                        To<br/><input id="price_specific_date_to_date_<?php echo $service['id'] ?>" class="form-control"  name="price_specific_date_to_date[]" value="" type="date" />
                    </div>
                    <div class="col-xs-2">
                    <button type="button" class="add_price_specific_date_price_remove_button ladda-button btn btn-danger" data-id="<?php echo $service['id'] ?>">
                        <span class="ladda-label"><i class="glyphicon glyphicon-trash"></i> Remove</span>
                    </button>
                    </div>
                </div>
                <?php
            }?>
        </div>
    </div>
</div>
<!-- // ADWAVE END -->