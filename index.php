<?php

$host = 'localhost:8999';
$db   = 'mint2adw_wp219';
$user = 'root';
$pass = 'root';

$charset = 'utf8mb4';

$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
$options = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];

function map_customer_appointment_mapping($row, $mapping, $increment) {
    return array(
        "ca_id" => $increment,
        "appointment_date" => "{$row['reserveDateFrom']}",
        "service_name" => "{$mapping['title']}",
        "service_price" => $row['price'],
        "service_tax" => number_format(($row['price']) * 0.13, 2),
        "wait_listed" => false,
        "deposit_format" => null,
        "number_of_persons" => 1,
        "units" => "1",
        "duration" => "3600",
        "staff_name" => "{$mapping['title']}",
        "extras" => array()
    );
}

function map_coupon_object($row, $mapping, $coupon_mapping, $increment) {
    $coupon = check_coupon($coupon_mapping, $row['coupon']);
    if ($coupon != 'null' && isset($coupon_mapping[$row['coupon']]) && isset($coupon_mapping[$row['coupon']]['discount'])) {
        $discount = $coupon_mapping[$row['coupon']]['discount'];
        return array(
            'code' => $row['coupon'],
            'discount' => $discount,
            'deduction' => '0.00'
        );
    }
    return null;
}
function map_customer_appointment_container_mapping ($row, $mapping, $coupon_mapping, $increment) {
    return array(
        "items" => array(map_customer_appointment_mapping($row, $mapping, $increment)),
        "coupon" => map_coupon_object($row, $mapping, $coupon_mapping, $increment),
        "subtotal" => array(
          "price" => $row['subtotalAmount'],
          "deposit" => 0
        ),
        "customer" => trim($row['name']),
        "tax_in_price" => "excluded",
        "tax_paid" => number_format($row['taxAmount'],2),
        "extras_multiply_nop" => 1
    );
}
function map_customer_appointment_container_mapping_makeup ($row, $mapping, $coupon_mapping, $increment, $makeup_mapping) {
    $makeup = $makeup_mapping[$row['data']['makeupTableAmount']];
    $items = array();
    $items[] = map_customer_appointment_mapping($row['data'], $mapping, $row['increment']);
    $items[] = array(
        "ca_id" => $row['makeup_increment1'],
        "appointment_date" => "{$row['data']['reserveDateFrom']}",
        "service_name" => $makeup['service_title'],
        "service_price" => $row['data']['price'],
        "service_tax" => number_format(($row['data']['price']) * 0.13, 2),
        "wait_listed" => false,
        "deposit_format" => null,
        "number_of_persons" => 1,
        "units" => $makeup['duration'],
        "duration" => "3600",
        "staff_name" => $makeup['service_title'],
        "extras" => array()
    );
    if ($makeup['both_seat']) {
        $items[] = array(
            "ca_id" => $row['makeup_increment2'],
            "appointment_date" => "{$row['data']['reserveDateFrom']}",
            "service_name" => $makeup['service_title2'],
            "service_price" => $row['data']['price'],
            "service_tax" => number_format(($row['data']['price']) * 0.13, 2),
            "wait_listed" => false,
            "deposit_format" => null,
            "number_of_persons" => 1,
            "units" => $makeup['duration'],
            "duration" => "3600",
            "staff_name" => $makeup['service_title2'],
            "extras" => array()
        );
    }
    return array(
        "items" => $items,
        "coupon" => map_coupon_object($row['data'], $mapping, $coupon_mapping, $increment),
        "subtotal" => array(
          "price" => $row['data']['subtotalAmount'],
          "deposit" => 0
        ),
        "customer" => trim($row['data']['name']),
        "tax_in_price" => "excluded",
        "tax_paid" => number_format($row['data']['taxAmount'],2),
        "extras_multiply_nop" => 1
    );
}
function bs_to_bookly_mapping () {
    return array(
        1 => array(
            'id' => 9,
            'title' => 'Ballroom',
            'staff_id' => 9
        ),
        2 => array(
            'id' => 10,
            'title' => 'Conservatory',
            'staff_id' => 10
        ),
        14 => array(
            'id' => 11,
            'title' => 'Library',
            'staff_id' => 11
        ),
        15 => array(
            'id' => 12,
            'title' => 'Lounge',
            'staff_id' => 12
        ),
        16 => array(
            'id' => 1,
            'title' => 'White Loft',
            'staff_id' => 2
        ),
        17 => array(
            'id' => 13,
            'title' => 'Nest',
            'staff_id' => 3
        )
    );
}

function map_reservation_to_bookly ($row, $mapping, $increment, &$makeup_rows, $makeup_mapping) {
    $result = array(
    'id' => $increment,
    'location_id' => 'null',
    'staff_id' => $mapping['staff_id'],
    'staff_any' => 0, //if not makeup room
    'service_id' => $mapping['id'],
    'custom_service_name' => 'null',
    'custom_service_price' => 'null',
    'start_date' => "'{$row['reserveDateFrom']}'",
    'end_date' => "'{$row['reserveDateTo']}'",
    'extras_duration' => 0,
    'internal_note' => 'null',
    'google_event_id' => 'null',
    'google_event_etag' => 'null',
    'outlook_event_id' => 'null',
    'outlook_event_change_key' => 'null',
    'outlook_event_series_id' => 'null',
    'created_from' => '\'bookly\'',
    'created' => "'" . date('Y-m-j H:i:s') . "'"
    );
    $query_start = 'INSERT INTO wp_bookly_appointments ( ' . implode(',', array_keys($result)) . ' )';
    $query_end = 'VALUES( ' . implode(',',array_values($result)) . ' );';

    echo $query_start . $query_end . '<br/>';

    if ($row['makeupTableAmount'] > 0) {
        $makeup_rows[] = array(
            'data' => $row,
            'increment' => $increment,
            'makeup_increment1' => $increment + 200,
            'makeup_increment2' => $increment + 300,
        );

        $makeup = $makeup_mapping[$row['makeupTableAmount']];

        $result2 = array(
        'id' => $increment + 200,
        'location_id' => 'null',
        'staff_id' => $makeup['staff_id'],
        'staff_any' => 0, //if not makeup room
        'service_id' => $makeup['service_id'],
        'custom_service_name' => 'null',
        'custom_service_price' => 'null',
        'start_date' => "'{$row['reserveDateFrom']}'",
        'end_date' => "'{$row['reserveDateTo']}'",
        'extras_duration' => 0,
        'internal_note' => 'null',
        'google_event_id' => 'null',
        'google_event_etag' => 'null',
        'outlook_event_id' => 'null',
        'outlook_event_change_key' => 'null',
        'outlook_event_series_id' => 'null',
        'created_from' => '\'bookly\'',
        'created' => "'" . date('Y-m-j H:i:s') . "'"
        );
        $query_start = 'INSERT INTO wp_bookly_appointments ( ' . implode(',', array_keys($result2)) . ' )';
        $query_end = 'VALUES( ' . implode(',',array_values($result2)) . ' );';

        echo $query_start . $query_end . '<br/>';

        if ($makeup['both_seat']) {
            $result3 = array(
                'id' => $increment + 300,
                'location_id' => 'null',
                'staff_id' => $makeup['staff_id2'],
                'staff_any' => 0, //if not makeup room
                'service_id' => $makeup['service_id2'],
                'custom_service_name' => 'null',
                'custom_service_price' => 'null',
                'start_date' => "'{$row['reserveDateFrom']}'",
                'end_date' => "'{$row['reserveDateTo']}'",
                'extras_duration' => 0,
                'internal_note' => 'null',
                'google_event_id' => 'null',
                'google_event_etag' => 'null',
                'outlook_event_id' => 'null',
                'outlook_event_change_key' => 'null',
                'outlook_event_series_id' => 'null',
                'created_from' => '\'bookly\'',
                'created' => "'" . date('Y-m-j H:i:s') . "'"
                );
                $query_start = 'INSERT INTO wp_bookly_appointments ( ' . implode(',', array_keys($result3)) . ' )';
                $query_end = 'VALUES( ' . implode(',',array_values($result3)) . ' );';

                echo $query_start . $query_end . '<br/>';
        }
    }
}

function map_customer_to_bookly ($row, $mapping, $increment) {
    $result = array(
    'id' => $increment,
    'wp_user_id' => 'null',
    'facebook_id' => 'null',
    'group_id' => 'null',
    'full_name' => "'" . trim($row['name']) . "'",
    'first_name' => "'" . trim($row['name']) . "'",
    'last_name' => "''",
    'phone' => "'{$row['phone']}'",
    'email' => "'{$row['email']}*'",
    'birthday' => 'null',
    'country' => "''",
    'state' => "''",
    'postcode' => "''",
    'city' => "''",
    'street' => "''",
    'street_number' => "''",
    'additional_address' => "''",
    'notes' => "''",
    'info_fields' => 'null',
    'credit' => 0,
    'created' => "'" . date('Y-m-j H:i:s') . "'"
    );
    $query_start = 'INSERT INTO wp_bookly_customers ( ' . implode(',', array_keys($result)) . ' )';
    $query_end = 'VALUES( ' . implode(',',array_values($result)) . ' );';

    echo $query_start . $query_end . '<br/>';
}

function check_coupon($coupon_mapping, $coupon) {
    $keys = array_keys($coupon_mapping);
    if (in_array($coupon, $keys) && isset($coupon_mapping[$coupon]) &&
     isset($coupon_mapping[$coupon]['id'])) {
        return $coupon_mapping[$coupon]['id'];
    }
    return 'null';
}

function check_payment_type($row, $coupon_mapping) {
    $coupon = check_coupon($coupon_mapping, $row['coupon']);
    if ($coupon != 'null') {
        $discount = $coupon_mapping[$row['coupon']]['discount'];
        if ($discount == 100) {
            return 'coupon';
        }
    }
    return 'stripe';
}

function map_payment_to_bookly ($row, $mapping, $coupon_mapping, $increment) {
    if ($row['makeupTableAmount'] < 1) {
        $result = array(
            'id' => $increment,
            'coupon_id' => check_coupon($coupon_mapping, $row['coupon']),
            'type' => "'" . check_payment_type($row, $coupon_mapping) . "'",
            'total' => $row['totalAmount'],
            'tax' => $row['taxAmount'],
            'paid' => $row['totalAmount'],
            'paid_type' => "'in_full'",
            'gateway_price_correction' => '0.00',
            'status' => "'completed'",
            'details' => "'" . json_encode(map_customer_appointment_container_mapping($row, $mapping, $coupon_mapping, $increment)) . "'",
            'created' => "'" . date('Y-m-j H:i:s') . "'"
            );
            $query_start = 'INSERT INTO wp_bookly_payments ( ' . implode(',', array_keys($result)) . ' )';
            $query_end = 'VALUES( ' . implode(',',array_values($result)) . ' );';

            echo $query_start . $query_end . '<br/>';
    } else {
        error_log('has make up ' . $increment);
    }

}

function map_payment_to_bookly_makeup ($row, $mapping, $coupon_mapping, $increment, $makeup_mapping) {
    $result = array(
        'id' => $row['increment'],
        'coupon_id' => check_coupon($coupon_mapping, $row['data']['coupon']),
        'type' => "'" . check_payment_type($row['data'], $coupon_mapping) . "'",
        'total' => $row['data']['totalAmount'],
        'tax' => $row['data']['taxAmount'],
        'paid' => $row['data']['totalAmount'],
        'paid_type' => "'in_full'",
        'gateway_price_correction' => '0.00',
        'status' => "'completed'",
        'details' => "'" . json_encode(map_customer_appointment_container_mapping_makeup($row, $mapping, $coupon_mapping, $increment, $makeup_mapping)) . "'",
        'created' => "'" . date('Y-m-j H:i:s') . "'"
    );

    $query_start = 'INSERT INTO wp_bookly_payments ( ' . implode(',', array_keys($result)) . ' )';
    $query_end = 'VALUES( ' . implode(',',array_values($result)) . ' );';

    echo $query_start . $query_end . '<br/>';
}

function generate_custom_field($photographer_first_name, $type_of_shoot) {
    return [
        array(
          "id" => 43107,
          "value" => $photographer_first_name
        ),
        array(
          "id" => 51989,
          "value" => $type_of_shoot
        ),
        array(
          "id" => 85403,
          "value" => ""
        ),
        array(
          "id" => 72188,
          "value" => [
            htmlentities("I agree to <a href=\"https://www.mint2.adwave.ca/Terms_and_Conditions.pdf?v=11122018\" target=\"_blank\">Terms and Conditions</a>")
          ]
        ),
        array(
          "id" => 89636,
          "value" => [
            "I agree there will be no more than 10 guests in the room per booking"
          ]
        ),
        array(
          "id" => 51014,
          "value" => " "
        ),
        array(
          "id" => 72029,
          "value" => []
        )
    ];
}
function map_customer_appointment_to_bookly ($row, $mapping, $increment) {
    $result = array(
    'id' => $increment,
    'series_id' => 'null',
    'package_id' => 'null',
    'customer_id' => $increment,
    'appointment_id' => $increment,
    'payment_id' => $increment,
    'number_of_persons' => 1,
    'units' => 1,
    'notes' => "'{$row['comments']}'",
    'extras' => "'[]'",
    'extras_multiply_nop' => '1',
    'extras_consider_duration' => '1',
    'custom_fields' => "'" . json_encode(generate_custom_field($row['photographer'], $row['shootType'])) . "'",
    'status' => "'approved'",
    'status_changed_at' => 'null',
    'token' => 'null',
    'time_zone' => 'null',
    'time_zone_offset' => 'null',
    'rating' => 'null',
    'rating_comment' => 'null',
    'locale' => 'null',
    'collaborative_service_id' => 'null',
    'collaborative_token' => 'null',
    'compound_service_id' => 'null',
    'compound_token' => 'null',
    'created_from' => "'frontend'",
    'created' => "'" . date('Y-m-j H:i:s') . "'"
    );
    $query_start = 'INSERT INTO wp_bookly_customer_appointments ( ' . implode(',', array_keys($result)) . ' )';
    $query_end = 'VALUES( ' . implode(',',array_values($result)) . ' );';

    echo $query_start . $query_end . '<br/>';
}

function map_customer_appointment_to_bookly_makeup ($row, $mapping, $increment, $makeup_mapping) {
    $makeup = $makeup_mapping[$row['data']['makeupTableAmount']];

    $result = array(
    'id' => $row['makeup_increment1'],
    'series_id' => 'null',
    'package_id' => 'null',
    'customer_id' => $row['increment'],
    'appointment_id' => $row['makeup_increment1'],
    'payment_id' => $row['increment'],
    'number_of_persons' => 1,
    'units' => $makeup['duration'],
    'notes' => "'{$row['data']['comments']}'",
    'extras' => "'[]'",
    'extras_multiply_nop' => '1',
    'extras_consider_duration' => '1',
    'custom_fields' => "'" . json_encode(generate_custom_field($row['data']['photographer'], $row['data']['shootType'])) . "'",
    'status' => "'approved'",
    'status_changed_at' => 'null',
    'token' => 'null',
    'time_zone' => 'null',
    'time_zone_offset' => 'null',
    'rating' => 'null',
    'rating_comment' => 'null',
    'locale' => 'null',
    'collaborative_service_id' => 'null',
    'collaborative_token' => 'null',
    'compound_service_id' => 'null',
    'compound_token' => 'null',
    'created_from' => "'frontend'",
    'created' => "'" . date('Y-m-j H:i:s') . "'"
    );
    $query_start = 'INSERT INTO wp_bookly_customer_appointments ( ' . implode(',', array_keys($result)) . ' )';
    $query_end = 'VALUES( ' . implode(',',array_values($result)) . ' );';

    echo $query_start . $query_end . '<br/>';

    if ($makeup['both_seat']) {
        $result = array(
            'id' => $row['makeup_increment2'],
            'series_id' => 'null',
            'package_id' => 'null',
            'customer_id' => $row['increment'],
            'appointment_id' => $row['makeup_increment1'],
            'payment_id' => $row['increment'],
            'number_of_persons' => 1,
            'units' => $makeup['duration'],
            'notes' => "'{$row['data']['comments']}'",
            'extras' => "'[]'",
            'extras_multiply_nop' => '1',
            'extras_consider_duration' => '1',
            'custom_fields' => "'" . json_encode(generate_custom_field($row['data']['photographer'], $row['data']['shootType'])) . "'",
            'status' => "'approved'",
            'status_changed_at' => 'null',
            'token' => 'null',
            'time_zone' => 'null',
            'time_zone_offset' => 'null',
            'rating' => 'null',
            'rating_comment' => 'null',
            'locale' => 'null',
            'collaborative_service_id' => 'null',
            'collaborative_token' => 'null',
            'compound_service_id' => 'null',
            'compound_token' => 'null',
            'created_from' => "'frontend'",
            'created' => "'" . date('Y-m-j H:i:s') . "'"
            );
            $query_start = 'INSERT INTO wp_bookly_customer_appointments ( ' . implode(',', array_keys($result)) . ' )';
            $query_end = 'VALUES( ' . implode(',',array_values($result)) . ' );';

            echo $query_start . $query_end . '<br/>';
    }
}

function map_coupon_to_bookly ($row, &$mapping, &$increment) {
    $keys = array_keys($mapping);

    if (!in_array($row['code'], $keys)) {
        return false;
    }

    $result = array(
    'id' => $increment,
    'code' => "'{$row['code']}'",
    'discount' => $row['value'],
    'deduction' => '0.00',
    'usage_limit' => 999999,
    'used' => 0,
    'once_per_customer' => 0,
    'date_limit_start' => 'null',
    'date_limit_end' => 'null',
    'min_appointments' => 1,
    'max_appointments' => 'null'
    );
    $query_start = 'INSERT INTO wp_bookly_coupons ( ' . implode(',', array_keys($result)) . ' )';
    $query_end = 'VALUES( ' . implode(',',array_values($result)) . ' );';

    echo $query_start . $query_end . '<br/>';
    $mapping[$row['code']] = array(
        'id' => $increment,
        'discount' => $row['value']
    );
    $increment++;
}

/**
 * Steps:
 * 1. Map the bookly old service to new ones
 * 2. Look through all appointments
 * 3. Insert appointment into bookly plugin
 * 4. Insert Customer into bookly
 * 5. If makeup room set, book those rooms
 * 6. Have a print mode and insert mode
 * 7. Get new coupons
 */
try {
    $increment = 1000;
    $coupon_increment = 50;
    //#6
    $action = (!empty($_GET['action']) ? $_GET['action'] : '');

    if (!($action == 'insert' || $action == 'print')) {
        die('Cannot insert or print statements');
    }
    $pdo = new PDO($dsn, $user, $pass, $options);


    //#1
    $mapping = bs_to_bookly_mapping();
    $coupon_mapping = array(
        'Pt_huge' => array("id" => 50, "discount" => 100),
        'PT_HUGE' => array("id" => 50, "discount" => 100),
        'cybermonday' => array(),
        'PT_HUGE2019' => array(),
        'CoolPeople' => array(),
        'info@joannagalant.com' => array(),
        'sophiachriskara@gmail.com1' => array(),
        'Mariam.patel98@gmail.com' => array(),
        'nicolecohen9@gmail.com' => array(),
        'delia.r.hansen@gmail.com' => array(),
        'MRNEWSLETTER' => array()
    );

    $makeup_mapping = array(
        20 => array(
            'duration' => 1,
            'service_id' => 14,
            'staff_id' => 4,
            'both_seat' => false,
            'seat2' => 0,
            'service_id2' => 0,
            'staff_id2' => 0,
            'service_title' => 'Make Up Table - Downstairs Seat 1',
            'service_title2' => 'Make Up Table - Upstairs Seat 1'
        ),
        40 => array(
            'duration' => 2,
            'service_id' => 14,
            'staff_id' => 4,
            'both_seat' => true,
            'service_id2' => 15,
            'staff_id2' => 13,
            'service_title' => 'Make Up Table - Downstairs Seat 1',
            'service_title2' => 'Make Up Table - Upstairs Seat 1'
        ),
        80 => array(
            'seat' => 1,
            'duration' => 2,
            'service_id' => 14,
            'staff_id' => 4,
            'both_seat' => true,
            'service_id2' => 15,
            'staff_id2' => 13,
            'service_title' => 'Make Up Table - Downstairs Seat 1',
            'service_title2' => 'Make Up Table - Upstairs Seat 1'
        )
    );

    $makeup_rows = array();
    $today = date('Y-m-d', time());

    $coupon_query = "SELECT * FROM `bs_coupons`;";

    $stmt = $pdo->query($coupon_query);

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
    {
        map_coupon_to_bookly($row, $coupon_mapping, $coupon_increment);
    }

    $reservation_query = "SELECT * FROM `bs_reservations_items` as i INNER JOIN `bs_reservations` as r ON i.reservationID = r.id WHERE i.reserveDateFrom > '{$today} 00:00:00' AND r.status=1;";

    $stmt = $pdo->query($reservation_query);

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
    {
        map_reservation_to_bookly($row, $mapping[$row['serviceID']], $increment, $makeup_rows, $makeup_mapping);
        map_customer_to_bookly($row, $mapping[$row['serviceID']], $increment);
        map_payment_to_bookly($row, $mapping[$row['serviceID']], $coupon_mapping, $increment);
        map_customer_appointment_to_bookly($row, $mapping[$row['serviceID']], $increment);
        $increment++;
    }
    echo '<br/><br/><br/><br/><br/>';
    foreach ($makeup_rows as $key => $row) {
        map_customer_appointment_to_bookly_makeup($row, $mapping[$row['data']['serviceID']], $increment, $makeup_mapping);
        map_payment_to_bookly_makeup($row, $mapping[$row['data']['serviceID']], $coupon_mapping, $increment, $makeup_mapping);
    }
} catch (\PDOException $e) {
     throw new \PDOException($e->getMessage(), (int)$e->getCode());
}