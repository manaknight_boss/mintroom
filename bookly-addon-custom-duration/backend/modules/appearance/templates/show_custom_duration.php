<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
use Bookly\Lib as BooklyLib;
?>
<div class="col-md-3">
    <div class="checkbox">
        <label>
            <input type="checkbox" id="bookly-show-custom-duration" <?php checked( get_option( 'bookly_custom_duration_enabled' ) ) ?>>
            <?php _e( 'Show custom duration', 'bookly' ) ?>
        </label>
    </div>
</div>
