<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<div class="col-md-3">
    <div class="checkbox">
        <label data-toggle="popover" data-trigger="hover" data-placement="auto" data-content="<?php echo esc_attr( 'Show custom duration required', 'bookly' ) ?>">
            <input type="checkbox" id="bookly-service-duration-with-price" <?php checked( get_option( 'bookly_app_service_duration_with_price' ) ) ?>>
            <?php _e( 'Show service price next to duration', 'bookly' ) ?>
        </label>
    </div>
</div>