<?php  if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
/** @var int $service_id */
?>
<label for="price_<?php echo $service_id ?>" class="bookly-js-unit-price-label" style="display: none;"><?php _e( 'Unit price', 'bookly' ) ?></label>
