<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
$site_key = '6LfuP6gUAAAAAG70PD4CQaqzttRps_IfP_HRhPBT';
$secret_key = '6LfuP6gUAAAAAM8PCMcsl1hOVZxdDFj5qJIa256i';
?>
    <?php 
    /* Adwave Custom - remove css */
    /*
    <link rel="stylesheet" href="/wp-admin/css/login.min.css">
    <link rel="stylesheet" href="/wp-content/themes/dt-the7/css/main.min.css?ver=7.2.0">
    */
    ?>
<?php if (isset($errors)) {
    echo '<div class="login" style="width:50%;"><div id="login_error">';
    echo '<strong>ERROR</strong>:';
    foreach ($errors as $key => $value) {
        echo $value;
    }
    echo '</div></div>';
}?>
<?php if (isset($success)) {
    echo '<div class="login" style="width:50%;">';
    echo '<p class="success">' . $success . '</p>';
    echo '</div>';
}?>
<div id="register-form" class="widecolumn">
<h3>Register</h3>
<form id="loginform" action="<?php echo plugins_url('post_register_new_user.php', __FILE__);?>" method="post">
    <p class="form-row">
        <label for="email"> Email <strong>*</strong></label><br/>
        <input type="text" class="input" name="email" id="email" required="true">
    </p>

    <p class="form-row">
        <label for="first_name"> First name<strong>*</strong></label><br/>
        <input type="text" class="input" name="first_name" id="first-name" required="true">
    </p>

    <p class="form-row">
        <label for="last_name"> Last name<strong>*</strong></label><br/>
        <input type="text" class="input" name="last_name" id="last-name" required="true">
    </p>

    <p class="form-row">
        <label for="password"> Password<strong>*</strong></label><br/>
        <input type="password" class="input" name="password" id="password" required="true">
    </p>

    <div class="recaptcha-container">
        <div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
    </div>
    <br/>
    <p class="signup-submit">
        <input type="submit" name="submit" class="register-button"
                value="Register"/>
    </p>
</form>
</div>
<script src='https://www.google.com/recaptcha/api.js'></script>
