<?php
use Bookly\Lib\Entities\Customer;
//use BooklyCustomFields\Lib\Captcha;
$site_key = '6LfuP6gUAAAAAG70PD4CQaqzttRps_IfP_HRhPBT';
$secret_key = '6LfuP6gUAAAAAM8PCMcsl1hOVZxdDFj5qJIa256i';

require_once( explode( "wp-content" , __FILE__ )[0] . "wp-load.php" );

function register_user( $email, $first_name, $last_name, $password ) {
    $errors = array();

    // Email address is used as both username and email. It is also the only
    // parameter we need to validate
    if ( ! is_email( $email ) ) {
        $errors[] = 'The email address you entered is not valid.';
        return $errors;
    }

    if ( username_exists( $email ) || email_exists( $email ) ) {
        $errors[] = 'An account exists with this email address.';
        return $errors;
    }

    // Generate the password so that the subscriber will have to check email...
    // $password = wp_generate_password( 12, false );

    $user_data = array(
        'user_login'    => $email,
        'user_email'    => $email,
        'user_pass'     => $password,
        'first_name'    => $first_name,
        'last_name'     => $last_name,
        'nickname'      => $first_name,
    );

    $user_id = wp_insert_user( $user_data );
    return $user_id;
}

function verify_recaptcha($secret_key) {
    // This field is set by the recaptcha widget if check is successful
    if ( isset ( $_POST['g-recaptcha-response'] ) ) {
        $captcha_response = $_POST['g-recaptcha-response'];
    } else {
        return false;
    }

    // Verify the captcha response from Google
    $response = wp_remote_post(
        'https://www.google.com/recaptcha/api/siteverify',
        array(
            'body' => array(
                'secret' => $secret_key,
                'response' => $captcha_response
            )
        )
    );

    $success = false;
    if ( $response && is_array( $response ) ) {
        $decoded_response = json_decode( $response['body'] );
        $success = $decoded_response->success;
    }

    return $success;
}

if (!empty($_POST)) {
    $email = $_POST['email'];
    $first_name = sanitize_text_field( $_POST['first_name'] );
    $last_name = sanitize_text_field( $_POST['last_name'] );
    $password = sanitize_text_field( $_POST['password'] );

    // Adwave Custom - Validate password strength
    $uppercase = preg_match('@[A-Z]@', $password);
    $lowercase = preg_match('@[a-z]@', $password);
    $number    = preg_match('@[0-9]@', $password);
    $specialChars = preg_match('@[^\w]@', $password);

    if(!$uppercase || !$lowercase || !$number || !$specialChars || strlen($password) < 8) {
        $errors = array('Password should be at least 8 characters in length that include at least one uppercase letter, one lowercase letter, one number, and one special character.');
        get_header();
        include_once 'register_new_user.php';
        get_footer();
        exit;        
    }
    // End Adwave Custom - Validate password strength

    if (!verify_recaptcha($secret_key)) {
        $errors = array('captcha failed');
        get_header();
        include_once 'register_new_user.php';
        get_footer();
        exit;
    }

    if (strlen($email) < 1 || strlen($first_name) < 1 || strlen($last_name) < 1 ||
     strlen($password) < 1) {
        $errors = array('All fields need to be filled in');
        get_header();
        include_once 'register_new_user.php';
        get_footer();
        exit;
     }
    $result = register_user( $email, $first_name, $last_name, $password );
    if ( is_array( $result ) ) {
        // Parse errors into a string and append as parameter to redirect
        $errors = $result;
        get_header();
        include_once 'register_new_user.php';
        get_footer();
    } else {
        include_once 'index.php';
        $plugin_class = new DWEmailVerify();
        $plugin_class->user_register($result);
    }
}
