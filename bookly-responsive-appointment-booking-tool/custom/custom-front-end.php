<?php

add_action( 'set_current_user', '__clear_cookie_after_login' );
function __clear_cookie_after_login() {
	$cookies = [
		'bookly-cst-full-name',
		'bookly-cst-email',
		'bookly-cst-email',
		'bookly-cst-phone',
		'bookly-cst-info-fields',
		'bookly-cst-birthday',
		'bookly-cst-first-name',
		'bookly-cst-last-name',
		'bookly-cst-country',
		'bookly-cst-state',
		'bookly-cst-postcode',
		'bookly-cst-city',
		'bookly-cst-street',
		'bookly-cst-street-number',
		'bookly-cst-additional-address',
	];

	foreach ( $cookies as $cookie ) {
		unset( $_COOKIE[ $cookie ] );
		setcookie( $cookie, '', time() - 3600 );
	}

	if ( session_status() === PHP_SESSION_ACTIVE ) {
		$__keys = [
			'full_name',
			'first_name',
			'last_name',
			'email',
			'email_confirm',
			'facebook_id',
			'phone',
			'birthday',
			'additional_address',
			'country',
			'state',
			'postcode',
			'city',
			'street',
			'street_number',
		];

		if ( isset( $_SESSION['bookly']['forms'] ) && is_array( $_SESSION['bookly']['forms'] ) ) {
			foreach ( $_SESSION['bookly']['forms'] as $name => $data ) {
				if ( ! isset( $data['data'] ) || ! is_array( $data['data'] ) ) {
					continue;
				}

				foreach ( $data['data'] as $key => $value ) {
					if ( in_array( $key, $__keys, true ) ) {
						unset( $data['data'][ $key ] );
					}
				}

				$_SESSION['bookly']['forms'][ $name ] = $data;
			}
		}
	}
}

/*add_action( 'wp_loaded', function () {
	dd( $_SESSION['bookly']['forms'] );
} );*/
