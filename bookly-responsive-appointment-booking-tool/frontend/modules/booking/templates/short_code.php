<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
use Bookly\Lib;
?>
<!--
Plugin Name: Bookly – Responsive WordPress Appointment Booking and Scheduling Plugin
Plugin URI: https://www.booking-wp-plugin.com/?utm_source=bookly_admin&utm_medium=plugins_page&utm_campaign=plugins_page
Version: <?php echo Lib\Plugin::getVersion() ?>
-->
<?php include '_css.php' ?>
<div id="bookly-form-<?php echo $form_id ?>" class="bookly-form" data-form_id="<?php echo $form_id ?>">
    <div style="text-align: center"><img src="<?php echo includes_url( 'js/tinymce/skins/lightgray/img/loader.gif' ) ?>" alt="<?php esc_attr_e( 'Loading...', 'bookly' ) ?>" /></div>
</div>
<span style="display:none;" class="mkd-bookly-option"><?php echo json_encode($bookly_options);?></span>
<span style="display:none;" class="mkd-bookly-seat-assignment"><?php echo json_encode(
    array(
    13 => array('service_id' => 15, 'seat' => 13, 'info_text' => 'Upstairs Make Up Tables - Seat 1'),
    14 => array('service_id' => 16, 'seat' => 14, 'info_text' => 'Upstairs Make Up Tables - Seat 2'),
    4 => array('service_id' => 14, 'seat' => 4, 'info_text' => 'Downstairs Make Up Tables - Seat 1'),
    5 => array('service_id' => 17, 'seat' => 5, 'info_text' => 'Downstairs Make Up Tables - Seat 2'),
    6 => array('service_id' => 18, 'seat' => 6, 'info_text' => 'Downstairs Make Up Tables - Seat 3'),
    7 => array('service_id' => 19, 'seat' => 7, 'info_text' => 'Downstairs Make Up Tables - Seat 4')
    )
);?></span>
<span style="display:none" class="mkd-bookly-competitive-days">
<?php
    $bookly_competitive_days = get_option('bookly_competitive_days', array());
    if (!$bookly_competitive_days) {
        $bookly_competitive_days = '[]';
    }
    if (!is_string($bookly_competitive_days)) {
        $bookly_competitive_days = json_encode($bookly_competitive_days);
    }

    echo $bookly_competitive_days;
?>
</span>
<span style="display:none;"><?php echo json_encode($sessions);?></span>
<script type="text/javascript">
    (function (win, fn) {
        var done = false, top = true,
            doc = win.document,
            root = doc.documentElement,
            modern = doc.addEventListener,
            add = modern ? 'addEventListener' : 'attachEvent',
            rem = modern ? 'removeEventListener' : 'detachEvent',
            pre = modern ? '' : 'on',
            init = function(e) {
                if (e.type == 'readystatechange') if (doc.readyState != 'complete') return;
                (e.type == 'load' ? win : doc)[rem](pre + e.type, init, false);
                if (!done) { done = true; fn.call(win, e.type || e); }
            },
            poll = function() {
                try { root.doScroll('left'); } catch(e) { setTimeout(poll, 50); return; }
                init('poll');
            };
        if (doc.readyState == 'complete') fn.call(win, 'lazy');
        else {
            if (!modern) if (root.doScroll) {
                try { top = !win.frameElement; } catch(e) { }
                if (top) poll();
            }
            doc[add](pre + 'DOMContentLoaded', init, false);
            doc[add](pre + 'readystatechange', init, false);
            win[add](pre + 'load', init, false);
        }
    })(window, function() {
        window.bookly( <?php echo json_encode( $bookly_options ) ?> );
    });
</script>