<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
use Bookly\Lib\Config;
use Bookly\Frontend\Modules\Booking\Proxy;
use Bookly\Lib\Utils;
?>
<?php echo $progress_tracker ?>
<div class="bookly-box"><?php echo $info_text ?>
<?php if ($makeup_room == 1) {?>
<select class="staff-chooser">
	<option value="<?php echo '8,13';?>" <?php echo ($service_id == 8) ? 'selected' : '';?>>Seat 1</option>
	<option value="<?php echo '9,14';?>" <?php echo ($service_id == 9) ? 'selected' : '';?>>Seat 2</option>
</select>
<?php } ?>
<?php if ($makeup_room == 2) {?>
<select class="staff-chooser">
	<option value="<?php echo '10,4';?>" <?php echo ($service_id == 10) ? 'selected' : '';?>>Seat 1</option>
	<option value="<?php echo '11,5';?>" <?php echo ($service_id == 11) ? 'selected' : '';?>>Seat 2</option>
<?php /*    
    <option value="<?php echo '12,6';?>" <?php echo ($service_id == 12) ? 'selected' : '';?>>Seat 3</option>
    <option value="<?php echo '13,7';?>" <?php echo ($service_id == 13) ? 'selected' : '';?>>Seat 4</option>
*/ ?>    
</select>
<?php } ?>
</div>
<div class="clear"></div>
<?php Proxy\Shared::renderWaitingListInfoText() ?>
<div class="bookly-box bookly-label-error"></div>
<?php if ( $has_slots ) : ?>
    <?php Proxy\Pro::renderTimeZoneSwitcher() ?>
<?php endif ?>
<?php if ( Config::showCalendar() ) : ?>
    <style type="text/css">
        .picker__holder{top: 0;left: 0;}
        .bookly-time-step {margin-left: 0;margin-right: 0;}
    </style>
    <div class="bookly-input-wrap bookly-slot-calendar bookly-js-slot-calendar">
         <input style="display: none" class="bookly-js-selected-date" type="text" value="" data-value="<?php echo esc_attr( $date ) ?>" />
    </div>
<?php endif ?>
<?php if ( $has_slots ) : ?>
    <div class="bookly-time-step">
        <!-- ADWAVE START -->
        <div class="mkd-main-spinner">
            <div style="text-align: center"><img src="<?php echo includes_url( 'js/tinymce/skins/lightgray/img/loader.gif' ) ?>" alt="<?php esc_attr_e( 'Loading...', 'bookly' ) ?>" /></div>
        </div>
        <div class="mkd-spinner">
        </div>
        <button class="bookly-time-next bookly-btn bookly-right ladda-button" data-style="zoom-in" data-spinner-size="40">
            <span class="ladda-label">Next<span class="week-hide-mobile"> Week</span></span>
        </button>
        <button class="bookly-time-prev bookly-btn bookly-right ladda-button" data-style="zoom-in" style="display: none" data-spinner-size="40">
            <span class="ladda-label">Previous<span class="week-hide-mobile"> Week</span></span>
        </button>
        <div id="bookly-go-to-date" class="bookly-input-wrap bookly-slot-calendar bookly-js-slot-calendar bookly-go-to-date ladda-buttons"  data-style="zoom-in" data-spinner-size="40">
            <span class="ladda-label" style="display:none;">Date</span>
            <input class="bookly-js-selected-date-future" type="text" value="" placeholder="Go to Date"/>
        </div>
        <div class="clear"></div>
        <!-- ADWAVE END -->
        <div class="bookly-columnizer-wrap">
            <div class="bookly-columnizer">
                <?php /* here _time_slots */ ?>
            </div>
        </div>
    </div>
<?php else : ?>
    <div class="bookly-not-time-screen<?php if ( ! Config::showCalendar() ) : ?> bookly-not-calendar<?php endif ?>">
        <?php _e( 'No time is available for selected criteria.', 'bookly' ) ?>
    </div>
<?php endif ?>
<div class="bookly-box bookly-nav-steps bookly-clear">
    <button class="bookly-back-step bookly-js-back-step bookly-btn ladda-button" data-style="zoom-in" data-spinner-size="40">
        <span class="ladda-label"><?php echo Utils\Common::getTranslatedOption( 'bookly_l10n_button_back' ) ?></span>
    </button>
    <?php if ( $show_cart_btn ) : ?>
        <?php Proxy\Cart::renderButton() ?>
    <?php endif ?>
    <?php if ( $has_slots ) : ?>
    <div class="<?php echo get_option( 'bookly_app_align_buttons_left' ) ? 'bookly-left' : 'bookly-right' ?>">
        <!-- Adwave Start -->
        <button class="bookly-time-go-to-cart bookly-btn bookly-right ladda-button" data-style="zoom-in" data-spinner-size="40">
            <span class="ladda-label">Add To Cart</span>
        </button>

        <!-- Adwave End -->
        <?php Proxy\Tasks::renderSkipButton( $userData ) ?>
    </div>
    <?php endif ?>
</div>
