<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
use Bookly\Backend\Components\Controls\Buttons;
use Bookly\Backend\Components\Controls\Inputs;
$bookly_competitive_days = get_option('bookly_competitive_days', array());
if (!$bookly_competitive_days) {
    $bookly_competitive_days = array();
} else {
    $bookly_competitive_days = json_decode($bookly_competitive_days, true);
}
?>
<!-- ADWAVE START -->
<div class="tab-pane" id="bookly_settings_competitive_days">
    <span id="template_competitive_days" style="display:none;">
    <div class="form-group">
            <label >Competitive Date Range &nbsp; <button type="button" class=" btn btn-danger" onclick="this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode)"><i class="glyphicon glyphicon-minus"></i> Remove</span></button></label><br/>
            <span>From:</span>
            <input  class="form-control" type="date" name="bookly_competitive_days_from[]" value="">
            <span>To:</span>
            <input  class="form-control" type="date" name="bookly_competitive_days_to[]" value="">
    </div>
    </span>
    <form method="post" action="<?php echo esc_url( add_query_arg( 'tab', 'competitive_days' ) ) ?>" id="purchase_code">
        <div class="form-group">
            <h4>Competitive Days</h4>
            <p>Please Choose days that the 4 hour and 8 hour discount don't apply.</p>
        </div>
        <div id="competitive_day_container">
            <?php
                foreach ($bookly_competitive_days as $key => $value) {
                    echo '<div class="form-group">';
                    echo '<label >Competitive Date Range &nbsp; <button type="button" class=" btn btn-danger" onclick="this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode)"><i class="glyphicon glyphicon-minus"></i> Remove</span></button></label><br/>';
                    echo '<span>From:</span>';
                    echo '<input class="form-control" type="date" name="bookly_competitive_days_from[]" value="' . $value['from'] . '">';
                    echo '<span>To:</span>';
                    echo '<input class="form-control" type="date" name="bookly_competitive_days_to[]" value="' . $value['to'] . '">';
                    echo '</div>';
                }
            ?>
        </div>
        <button type="button" id="add_competitive_day" class=" btn btn-success">
            <i class="glyphicon glyphicon-plus"></i> Add Competitive Days</span>
        </button>
        <br/>
        <br/>
        <br/>
        <div class="panel-footer">
            <?php Inputs::renderCsrf() ?>
            <?php Buttons::renderSubmit() ?>
            <?php Buttons::renderReset() ?>
        </div>
    </form>
</div>

<script>
    var competitive_days = <?php echo json_encode($bookly_competitive_days);?>;
    if (competitive_days.length < 1) {
        document.getElementById('competitive_day_container').innerHTML =
        document.getElementById('competitive_day_container').innerHTML +
        document.getElementById('template_competitive_days').innerHTML;
    }

    document.getElementById('add_competitive_day').addEventListener("click", function(){
        document.getElementById('competitive_day_container').insertAdjacentHTML('beforeend',
        document.getElementById('template_competitive_days').innerHTML);
    });
</script>

<!-- ADWAVE END -->