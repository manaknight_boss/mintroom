<?php

namespace Bookly\Backend\Modules\Appointments;

use Bookly\Lib\Entities\Customer;

class Utils {
	/**
	 * Query customers and return customer IDs.
	 *
	 * @param string $term  The search term.
	 * @param int    $limit Limit the search results.
	 * @return array
	 */
	public static function searchCustomers( $term, $limit = 0 ) {
		// Apply the filter to allow users custom the results.
		$results = apply_filters( 'bookly_pre_search_customers', false, $term, $limit );

		// If custom search results available, just return it.
		if ( is_array( $results ) ) {
			return $results;
		}

		global $wpdb;
		$customer_table = $wpdb->prefix . 'bookly_customers';

		add_action( 'pre_user_query', function ( \WP_User_Query $query ) use ( $wpdb, $customer_table ) {
			$query->query_from .= " INNER JOIN `{$customer_table}` ON `{$customer_table}`.`wp_user_id` = {$wpdb->prefix}users.ID";
		} );

		add_filter( 'user_search_columns', function ( $search_columns ) use ( $customer_table ) {
			return array_merge( $search_columns, [
				$customer_table . '.full_name',
				$customer_table . '.email',
				$customer_table . '.phone',
			] );
		} );

		$query = new \WP_User_Query( apply_filters( 'bookly_customer_search_query', [
			'fields' => 'ID',
			'number' => $limit,
			'search' => '*' . esc_attr( $term ) . '*',
			'search_columns' => [ 'user_login', 'user_nicename' ],
		], $term, $limit, 'main_query' ) );


		// Merge the both results.
		$results = wp_parse_id_list( (array) $query->get_results() );

		// Limit the results.
		if ( $limit && count( $results ) > $limit ) {
			$results = array_slice( $results, 0, $limit );
		}

		return $results;
	}

	public static function getCustomersData( $ids ) {
		global $wpdb;

		$customer_table = $wpdb->prefix . 'bookly_customers';

		$ids = implode( ',', wp_parse_id_list( $ids ) );

		// Avoid perform query if empty ids.
		if ( empty( $ids ) ) {
			return [];
		}

		$customers = $wpdb->get_results(
			"SELECT id, wp_user_id, full_name, first_name, last_name, email, phone FROM `{$customer_table}` WHERE `wp_user_id` IN ({$ids})",
			OBJECT
		);

		$response = [];
		foreach ( $customers as $item ) {
			$response[ $item->wp_user_id ] = $item;
		}

		return $response;
	}
}
