<?php
add_action('wp_footer', 'mycustom_mr_footer');
function mycustom_mr_footer() {
    ?>
    <script type="text/javascript">
        document.addEventListener( 'wpcf7mailsent', function( event ) {
            location.replace('https://www.mintroom.ca/thank-you-page-contact/');
        }, false );
    </script>
    <?php
}


// ADWAVE START
add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
	if (!current_user_can('administrator') && !is_admin()) {
	  show_admin_bar(false);
	}
}
function restrict_admin_with_redirect() {
    if ( ! current_user_can( 'manage_options' ) && ( ! wp_doing_ajax() ) ) {
        wp_safe_redirect( '/appointments' ); // Replace this with the URL to redirect to.
        exit;
    }
}
add_action( 'admin_init', 'restrict_admin_with_redirect', 1 );
// ADWAVE END

// ADWAVE CUSTOM PASSWORD RESET EMAIL
add_filter('wp_mail_from', 'itsg_mail_from_address');
function itsg_mail_from_address($email){
return 'no-reply@mintroom.ca';
}
add_filter('wp_mail_from_name', 'itsg_mail_from_name');
function itsg_mail_from_name($from_name){
return "Mint Room Studios";
}
// END ADWAVE

// ADWAVE CHANGE ADMIN LOGO AND PAGE TITLE
function my_login_logo_one() { 
?> 
<style type="text/css"> 
body.login div#login h1 a {
 background-image: url(https://www.mint2.adwave.ca/wp-content/uploads/2018/01/mr150-150x133.png);
padding-bottom: 30px;
background-size: contain;
width: 100%;
} 
</style>
 <?php 
} add_action( 'login_enqueue_scripts', 'my_login_logo_one' );

function custom_login_title( $login_title ) {
return str_replace(array( ' &lsaquo;', ' &#8212; WordPress'), array( ' &bull;', ''),$login_title );
}
add_filter( 'login_title', 'custom_login_title' );

// ADWAVE CHANGE USERNAME OR EMAIL ADDRESS TO EMAIL ADDRESS
add_filter(  'gettext',  'register_text'  );
add_filter(  'ngettext',  'register_text'  );
function register_text( $translating ) {
     $translated = str_ireplace(  'Username or Email Address',  'Email Address',  $translating );
     return $translated;
}

add_filter( 'retrieve_password_message', 'my_retrieve_password_message', 10, 4 );
function my_retrieve_password_message( $message, $key, $user_login, $user_data ) {

    // Start with the default content.
    $site_name = wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES );
    $message = __( 'You have requested a password reset for your Mintroom Studios account.' );
    $message .= "\r\n\r\n";
    /* translators: %s: user login */
    $message .= __( 'If this was a mistake, just ignore this email and nothing will happen.' ) . "\r\n\r\n";
    $message .= __( 'To reset your password, visit the following address:' ) . "\r\n\r\n";
    $message .= '<' . network_site_url( "wp-login.php?action=rp&key=$key&login=" . rawurlencode( $user_login ), 'login' ) . ">\r\n";

    /*
     * If the problem persists with this filter, remove
     * the last line above and use the line below by
     * removing "//" (which comments it out) and hard
     * coding the domain to your site, thus avoiding
     * the network_site_url() function.
     */
    // $message .= '<http://yoursite.com/wp-login.php?action=rp&key=' . $key . '&login=' . rawurlencode( $user_login ) . ">\r\n";

    // Return the filtered message.
    return $message;

}
// ADWAVE END

// function log_sql_queries($text_query){
//      //Uncomment me if you want a lot of info about where the sql query comes from and what action started it off
//     $traces = debug_backtrace();
//     foreach ($traces as $tobj => $trace) {
//         if($trace['function'] == 'do_action'){
//             $args = $trace['args'];
//         }
//         error_log("TRACE:$i:"  . $trace['function'] . print_r($args,1));
//         $i++;
//     }
    
//     error_log("INFO:SQL: " . $text_query);
//     return $text_query;
// }
// add_filter( 'posts_request', 'log_sql_queries', 500 );


add_filter( 'wp_mail', 'my_wp_mail_filter' );
function my_wp_mail_filter( $args ) {
    
    $to = $args['to'];
    if ( ! is_array( $to ) ) {
        $to = explode( ',', $to );
    }
    foreach($to as $key=>$value){
      $to[$key]=str_replace("*","",$value);
    }
 
    $new_wp_mail = array(
        'to'          => $to, 
        'subject'     => $args['subject'],
        'message'     => $args['message'],
        'headers'     => $args['headers'],
        'attachments' => $args['attachments'],
    );
    
    return $new_wp_mail;
}