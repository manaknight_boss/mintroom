<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
/*
Plugin Name: Bookly Customer Cabinet (Add-on)
Plugin URI: https://www.booking-wp-plugin.com/?utm_source=bookly_admin&utm_medium=plugins_page&utm_campaign=plugins_page
Description: Bookly Customer Cabinet add-on allows your customers view and manage their personal details and appointments list in a user account on the front-end.
Version: 1.6
Author: Bookly
Author URI: https://www.booking-wp-plugin.com/?utm_source=bookly_admin&utm_medium=plugins_page&utm_campaign=plugins_page
Text Domain: bookly
Domain Path: /languages
License: Commercial
*/

if ( ! function_exists( 'bookly_customer_cabinet_loader' ) ) {
    include_once __DIR__ . '/autoload.php';

    BooklyCustomerCabinet\Lib\Boot::up();
}