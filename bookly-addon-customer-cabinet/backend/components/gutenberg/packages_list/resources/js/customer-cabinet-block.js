(function (wp, $) {
    var el = wp.element.createElement,
        components        = wp.components,
        blockControls     = wp.editor.BlockControls,
        inspectorControls = wp.editor.InspectorControls,
        attributes = {
            short_code: {
                type: 'string',
                default: '[bookly-customer-cabinet]'
            },
            tab_appointments: {
                type: 'boolean',
                default: true
            },
            tab_profile: {
                type: 'boolean',
                default: true
            },
            appointments_date: {
                type: 'boolean',
                default: true
            },
            appointments_timezone: {
                type: 'boolean',
                default: true
            },
            appointments_category: {
                type: 'boolean',
                default: true
            },
            appointments_service: {
                type: 'boolean',
                default: true
            },
            appointments_staff: {
                type: 'boolean',
                default: true
            },
            appointments_price: {
                type: 'boolean',
                default: true
            },
            appointments_status: {
                type: 'boolean',
                default: true
            }
        };

    $.each(BooklyCustomerCabinetL10n.customFields, function (index, field) {
        if(field.type !== 'file') {
            var attr_name = 'custom_field_' + field.id;
            attributes[attr_name] = {
                type: 'boolean',
                default: false
            };
        }
    });

    attributes.appointments_cancel = {
        type: 'boolean',
        default: true
    };
    attributes.appointments_reschedule = {
        type: 'boolean',
        default: true
    };
    attributes.profile_name = {
        type: 'boolean',
        default: true
    };
    attributes.profile_email = {
        type: 'boolean',
        default: true
    };
    attributes.profile_phone = {
        type: 'boolean',
        default: true
    };
    attributes.profile_birthday = {
        type: 'boolean',
        default: true
    };
    attributes.profile_address = {
        type: 'boolean',
        default: true
    };
    attributes.profile_wp_password = {
        type: 'boolean',
        default: true
    };
    $.each(BooklyCustomerCabinetL10n.customerInformation, function (index, field) {
        var attr_name = 'customer_information_' + field.id;
        attributes[attr_name] = {
            type: 'boolean',
            default: false
        };
    });
    attributes.profile_delete = {
        type: 'boolean',
        default: true
    };

    wp.blocks.registerBlockType('bookly/customer-cabinet', {
        title: BooklyCustomerCabinetL10n.block.title,
        description: BooklyCustomerCabinetL10n.block.description,
        icon: el('svg', { width: '20', height: '20', viewBox: "0 0 64 64" },
            el('path', {style: {fill: "rgb(0, 0, 0)"}, d: "M 8 0 H 56 A 8 8 0 0 1 64 8 V 22 H 0 V 8 A 8 8 0 0 1 8 0 Z"}),
            el('path', {style: {fill: "rgb(244, 102, 47)"}, d: "M 0 22 H 64 V 56 A 8 8 0 0 1 56 64 H 8 A 8 8 0 0 1 0 56 V 22 Z"}),
            el('rect', {style: {fill: "rgb(98, 86, 86)"}, x: 6, y: 6, width: 52, height: 10}),
            el('rect', {style: {fill: "rgb(242, 227, 227)"}, x: 12, y: 30, width: 40, height: 24}),
            el('ellipse', {style: {fill: "rgb(221, 224, 233)", stroke: 'rgb(0, 0, 0)'}, cx: 44, cy: 48, rx: 16, ry: 12}),
        ),
        category: 'bookly-blocks',
        keywords: [
            'bookly',
            'ratings',
        ],
        supports: {
            customClassName: false,
            html: false
        },
        attributes: attributes,
        edit: function (props) {
            var inspectorTabAppointmentsElements = [],
                inspectorTabProfileElements = [],
                attributes   = props.attributes
            ;

            function getShortCode(props, attributes) {
                var short_code = '[bookly-customer-cabinet',
                    tabs = [],
                    appointments= [],
                    profile = [];

                if (attributes.tab_appointments) {
                    tabs.push('appointments');
                }
                if (attributes.tab_profile) {
                    tabs.push('profile');
                }
                if (tabs.length > 0) {
                    short_code += ' tabs="' + tabs.join(',') + '"';
                }

                if (attributes.tab_appointments) {
                    if (attributes.appointments_date) {
                        appointments.push('date');
                    }
                    if (attributes.appointments_timezone) {
                        appointments.push('timezone');
                    }
                    if (attributes.appointments_category) {
                        appointments.push('category');
                    }
                    if (attributes.appointments_service) {
                        appointments.push('service');
                    }
                    if (attributes.appointments_staff) {
                        appointments.push('staff');
                    }
                    if (attributes.appointments_price) {
                        appointments.push('price');
                    }
                    if (attributes.appointments_status) {
                        appointments.push('status');
                    }
                    $.each(BooklyCustomerCabinetL10n.customFields, function (index, field) {
                        if(field.type !== 'file') {
                            var attr_name = 'custom_field_' + field.id;
                            if (attributes[attr_name]) {
                                appointments.push(attr_name)
                            }
                        }
                    });
                    if (attributes.appointments_cancel) {
                        appointments.push('cancel');
                    }
                    if (attributes.appointments_reschedule) {
                        appointments.push('reschedule');
                    }
                    if (appointments.length > 0) {
                        short_code += ' appointments="' + appointments.join(',') + '"';
                    }
                }

                if (attributes.tab_profile) {
                    if (attributes.profile_name) {
                        profile.push('name');
                    }
                    if (attributes.profile_email) {
                        profile.push('email');
                    }
                    if (attributes.profile_phone) {
                        profile.push('phone');
                    }
                    if (attributes.profile_birthday) {
                        profile.push('birthday');
                    }
                    if (attributes.profile_address) {
                        profile.push('address');
                    }
                    if (attributes.profile_wp_password) {
                        profile.push('wp_password');
                    }
                    $.each(BooklyCustomerCabinetL10n.customerInformation, function (index, field) {
                        var attr_name = 'customer_information_' + field.id;
                        if (attributes[attr_name]) {
                            profile.push(attr_name)
                        }
                    });
                    if (attributes.profile_delete) {
                        profile.push('delete');
                    }
                    if (profile.length > 0) {
                        short_code += ' profile="' + profile.join(',') + '"';
                    }
                }

                short_code += ']';

                props.setAttributes({short_code: short_code});

                return short_code;
            }

            inspectorTabAppointmentsElements.push(el(components.PanelRow,
                {},
                el('label', {htmlFor: 'bookly-js-hide-tab-appointments'}, BooklyCustomerCabinetL10n.Show),
                el(components.FormToggle, {
                    id: 'bookly-js-hide-tab-appointments',
                    checked: attributes.tab_appointments,
                    onChange: function () {
                        return props.setAttributes({tab_appointments: !props.attributes.tab_appointments});
                    },
                })
            ));
            if(props.attributes.tab_appointments) {
                inspectorTabAppointmentsElements.push(el(components.PanelRow,
                    {},
                    el('label', {htmlFor: 'bookly-js-hide-date'}, BooklyCustomerCabinetL10n.appointment.date),
                    el(components.FormToggle, {
                        id: 'bookly-js-hide-date',
                        checked: attributes.appointments_date,
                        onChange: function () {
                            return props.setAttributes({appointments_date: !props.attributes.appointments_date});
                        },
                    })
                ));
                inspectorTabAppointmentsElements.push(el(components.PanelRow,
                    {},
                    el('label', {htmlFor: 'bookly-js-hide-timezone'}, BooklyCustomerCabinetL10n.appointment.timezone),
                    el(components.FormToggle, {
                        id: 'bookly-js-hide-timezone',
                        checked: attributes.appointments_timezone,
                        onChange: function () {
                            return props.setAttributes({appointments_timezone: !props.attributes.appointments_timezone});
                        },
                    })
                ));
                inspectorTabAppointmentsElements.push(el(components.PanelRow,
                    {},
                    el('label', {htmlFor: 'bookly-js-hide-category'}, BooklyCustomerCabinetL10n.appointment.category),
                    el(components.FormToggle, {
                        id: 'bookly-js-hide-category',
                        checked: attributes.appointments_category,
                        onChange: function () {
                            return props.setAttributes({appointments_category: !props.attributes.appointments_category});
                        },
                    })
                ));
                inspectorTabAppointmentsElements.push(el(components.PanelRow,
                    {},
                    el('label', {htmlFor: 'bookly-js-hide-service'}, BooklyCustomerCabinetL10n.appointment.service),
                    el(components.FormToggle, {
                        id: 'bookly-js-hide-service',
                        checked: attributes.appointments_service,
                        onChange: function () {
                            return props.setAttributes({appointments_service: !props.attributes.appointments_service});
                        },
                    })
                ));
                inspectorTabAppointmentsElements.push(el(components.PanelRow,
                    {},
                    el('label', {htmlFor: 'bookly-js-hide-staff'}, BooklyCustomerCabinetL10n.appointment.staff),
                    el(components.FormToggle, {
                        id: 'bookly-js-hide-staff',
                        checked: attributes.appointments_staff,
                        onChange: function () {
                            return props.setAttributes({appointments_staff: !props.attributes.appointments_staff});
                        },
                    })
                ));
                inspectorTabAppointmentsElements.push(el(components.PanelRow,
                    {},
                    el('label', {htmlFor: 'bookly-js-hide-price'}, BooklyCustomerCabinetL10n.appointment.price),
                    el(components.FormToggle, {
                        id: 'bookly-js-hide-price',
                        checked: attributes.appointments_price,
                        onChange: function () {
                            return props.setAttributes({appointments_price: !props.attributes.appointments_price});
                        },
                    })
                ));
                inspectorTabAppointmentsElements.push(el(components.PanelRow,
                    {},
                    el('label', {htmlFor: 'bookly-js-hide-status'}, BooklyCustomerCabinetL10n.appointment.status),
                    el(components.FormToggle, {
                        id: 'bookly-js-hide-status',
                        checked: attributes.appointments_status,
                        onChange: function () {
                            return props.setAttributes({appointments_status: !props.attributes.appointments_status});
                        },
                    })
                ));

                $.each(BooklyCustomerCabinetL10n.customFields, function (index, field) {
                    if(field.type !== 'file') {
                        var attr_name = 'custom_field_' + field.id,
                            attribute = {};
                        attribute[attr_name] = !props.attributes[attr_name];
                        inspectorTabAppointmentsElements.push(el(components.PanelRow,
                            {},
                            el('label', {htmlFor: 'bookly-js-show-custom-field-' + field.id}, field.label),
                            el(components.FormToggle, {
                                id: 'bookly-js-show-custom-field-' + field.id,
                                checked: attributes[attr_name],
                                onChange: function () {
                                    return props.setAttributes(attribute);
                                },

                            })
                        ));
                        inspectorTabAppointmentsElements.push(el('div', {style: {'margin': '-5px 0 1.5em 0', 'font-style': 'italic'}}, BooklyCustomerCabinetL10n.appointment.customField));
                    }
                });

                inspectorTabAppointmentsElements.push(el(components.PanelRow,
                    {},
                    el('label', {htmlFor: 'bookly-js-hide-cancel'}, BooklyCustomerCabinetL10n.appointment.cancel),
                    el(components.FormToggle, {
                        id: 'bookly-js-hide-cancel',
                        checked: attributes.appointments_cancel,
                        onChange: function () {
                            return props.setAttributes({appointments_cancel: !props.attributes.appointments_cancel});
                        },
                    })
                ));
                inspectorTabAppointmentsElements.push(el(components.PanelRow,
                    {},
                    el('label', {htmlFor: 'bookly-js-hide-reschedule'}, BooklyCustomerCabinetL10n.appointment.reschedule),
                    el(components.FormToggle, {
                        id: 'bookly-js-hide-reschedule',
                        checked: attributes.appointments_reschedule,
                        onChange: function () {
                            return props.setAttributes({appointments_reschedule: !props.attributes.appointments_reschedule});
                        },
                    })
                ));
            }

            inspectorTabProfileElements.push(el(components.PanelRow,
                {},
                el('label', {htmlFor: 'bookly-js-hide-tab-profile'}, BooklyCustomerCabinetL10n.Show),
                el(components.FormToggle, {
                    id: 'bookly-js-hide-tab-profile',
                    checked: attributes.tab_profile,
                    onChange: function () {
                        return props.setAttributes({tab_profile: !props.attributes.tab_profile});
                    },
                })
            ));

            if (attributes.tab_profile) {
                inspectorTabProfileElements.push(el(components.PanelRow,
                    {},
                    el('label', {htmlFor: 'bookly-js-hide-name'}, BooklyCustomerCabinetL10n.profile.name),
                    el(components.FormToggle, {
                        id: 'bookly-js-hide-name',
                        checked: attributes.profile_name,
                        onChange: function () {
                            return props.setAttributes({profile_name: !props.attributes.profile_name});
                        },
                    })
                ));
                inspectorTabProfileElements.push(el(components.PanelRow,
                    {},
                    el('label', {htmlFor: 'bookly-js-hide-email'}, BooklyCustomerCabinetL10n.profile.email),
                    el(components.FormToggle, {
                        id: 'bookly-js-hide-email',
                        checked: attributes.profile_email,
                        onChange: function () {
                            return props.setAttributes({profile_email: !props.attributes.profile_email});
                        },
                    })
                ));
                inspectorTabProfileElements.push(el(components.PanelRow,
                    {},
                    el('label', {htmlFor: 'bookly-js-hide-phone'}, BooklyCustomerCabinetL10n.profile.phone),
                    el(components.FormToggle, {
                        id: 'bookly-js-hide-phone',
                        checked: attributes.profile_phone,
                        onChange: function () {
                            return props.setAttributes({profile_phone: !props.attributes.profile_phone});
                        },
                    })
                ));
                inspectorTabProfileElements.push(el(components.PanelRow,
                    {},
                    el('label', {htmlFor: 'bookly-js-hide-birthday'}, BooklyCustomerCabinetL10n.profile.birthday),
                    el(components.FormToggle, {
                        id: 'bookly-js-hide-birthday',
                        checked: attributes.profile_birthday,
                        onChange: function () {
                            return props.setAttributes({profile_birthday: !props.attributes.profile_birthday});
                        },
                    })
                ));
                inspectorTabProfileElements.push(el(components.PanelRow,
                    {},
                    el('label', {htmlFor: 'bookly-js-hide-address'}, BooklyCustomerCabinetL10n.profile.address),
                    el(components.FormToggle, {
                        id: 'bookly-js-hide-address',
                        checked: attributes.profile_address,
                        onChange: function () {
                            return props.setAttributes({profile_address: !props.attributes.profile_address});
                        },
                    })
                ));
                inspectorTabProfileElements.push(el(components.PanelRow,
                    {},
                    el('label', {htmlFor: 'bookly-js-hide-wp-password'}, BooklyCustomerCabinetL10n.profile.wordpressPassword),
                    el(components.FormToggle, {
                        id: 'bookly-js-hide-wp-password',
                        checked: attributes.profile_wp_password,
                        onChange: function () {
                            return props.setAttributes({profile_wp_password: !props.attributes.profile_wp_password});
                        },
                    })
                ));
                $.each(BooklyCustomerCabinetL10n.customerInformation, function (index, field) {
                    var attr_name = 'customer_information_' + field.id,
                        attribute = {};
                    attribute[attr_name] = !props.attributes[attr_name];
                    inspectorTabProfileElements.push(el(components.PanelRow,
                        {},
                        el('label', {htmlFor: 'bookly-js-show-customer-info-' + field.id}, field.label),
                        el(components.FormToggle, {
                            id: 'bookly-js-show-customer-info-' + field.id,
                            checked: attributes[attr_name],
                            onChange: function () {
                                return props.setAttributes(attribute);
                            },

                        })
                    ));
                    inspectorTabProfileElements.push(el('div', {style: {'margin': '-5px 0 1.5em 0', 'font-style': 'italic'}}, BooklyCustomerCabinetL10n.profile.customerInformation));
                });
                inspectorTabProfileElements.push(el(components.PanelRow,
                    {},
                    el('label', {htmlFor: 'bookly-js-hide-delete-account'}, BooklyCustomerCabinetL10n.profile.deleteAccount),
                    el(components.FormToggle, {
                        id: 'bookly-js-hide-delete-account',
                        checked: attributes.profile_delete,
                        onChange: function () {
                            return props.setAttributes({profile_delete: !props.attributes.profile_delete});
                        },
                    })
                ));
            }

            return [
                el(blockControls, {key: 'controls'}),
                el(inspectorControls, {key: 'inspector'},
                    el(components.PanelBody, {initialOpen: true, title: BooklyCustomerCabinetL10n.appointmentManagement},
                        inspectorTabAppointmentsElements
                    ),
                    el(components.PanelBody, {initialOpen: true, title: BooklyCustomerCabinetL10n.profileManagement},
                        inspectorTabProfileElements
                    ),
                ),
                el('div', {},
                    getShortCode(props, props.attributes)
                )
            ]
        },

        save: function (props) {
            return (
                el('div', {},
                    props.attributes.short_code
                )
            )
        }
    })
})(
    window.wp,
    jQuery
);