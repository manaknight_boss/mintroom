<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
use Bookly\Backend\Components\Controls\Buttons;
?>
<div id="bookly-customer-cabinet-reschedule-dialog" class="modal fade" tabindex=-1 role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="modal-title h2"><?php _e( 'Reschedule', 'bookly' ) ?></div>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger collapse" id="bookly-reschedule-error"></div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="bookly-reschedule-date"><?php _e( 'Date', 'bookly' ) ?></label>
                                <input id="bookly-reschedule-date" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="bookly-reschedule-time"><?php _e( 'Time', 'bookly' ) ?></label>
                                <select id="bookly-reschedule-time" class="form-control"></select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div>
                        <?php Buttons::renderSubmit() ?>
                        <?php Buttons::renderCustom( null, 'btn-lg btn-default', __( 'Cancel', 'bookly' ), array( 'data-dismiss' => 'modal' ) ) ?>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
