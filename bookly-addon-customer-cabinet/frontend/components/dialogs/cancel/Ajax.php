<?php
namespace BooklyCustomerCabinet\Frontend\Components\Dialogs\Cancel;

use Bookly\Lib as BooklyLib;

/**
 * Class Ajax
 * @package BooklyCustomerCabinet\Frontend\Components\Dialogs\Cancel
 */
class Ajax extends BooklyLib\Base\Ajax
{
    /**
     * @inheritdoc
     */
    protected static function permissions()
    {
        return array(
            '_default' => 'user',
        );
    }

    /**
     * Cancel appointment
     */
    public static function cancelAppointment()
    {
    	/** @var \Bookly\Lib\Entities\Customer $customer */
        $customer    = BooklyLib\Entities\Customer::query()->where( 'wp_user_id', get_current_user_id() )->findOne();
        $customer_id = $customer->getId();

        $ca_id = self::parameter( 'ca_id' );

        $ca = BooklyLib\Entities\CustomerAppointment::find( $ca_id );
        if ( $ca->getCustomerId() == $customer_id ) {
            $appointment = new BooklyLib\Entities\Appointment();
            if ( $appointment->load( $ca->getAppointmentId() ) ) {
                //ADWAVE START
                /**
                 * Steps:
                 * 1. 48 hours+ before date of cancellation gives you 80% credit for your next booking
                 * 2. Between 24 to 48 hours before date of cancellation gives you 50% credit for your next booking
                 * 3. Anything under 24 hours before date of cancellation is not possible
                 */
                $payment_id = $ca->getPaymentId();
                if ($payment_id) {
                    $payment = new BooklyLib\Entities\Payment();
                    $payment->load($payment_id);
                    $p_details = json_decode($payment->getDetails(), true);
                    $total = 0;
                    $count = 0;
                    $row_duration = 1;

                    if (!$payment->getCouponId()) {
                        foreach ($p_details['items'] as $key => $item) {
                            if ($item['ca_id'] == $ca->getId()) {
                                $total = $item['service_price']  + ((float)$item['service_tax']);
                                $row_duration = (int)$item['units'];
                            }
                            if ($item['units'] == 1 || $item['units'] == '1') {
                                $count++;
                            } else {
                                $count = $count + ((int)$item['units']);
                            }
                        }
                    } else {
                        $coupon_discount = 0;
                        $coupon_deduction = 0;
                        if (isset($p_details['coupon']) && isset($p_details['coupon']['code'])) {
                            $coupon_discount = (int)$p_details['coupon']['discount'];
                            $coupon_deduction = (int)$p_details['coupon']['deduction'];
                        }
                        $total = 0;
                        $real_total = 0;
                        foreach ($p_details['items'] as $key => $item) {
                            $real_total += $item['service_price'];
                            if ($item['ca_id'] == $ca->getId()) {
                                $total = $item['service_price'];
                                //Adwave
                                $affected_by_coupon = $item['affected_by_coupon'] === true || !isset($item['affected_by_coupon']);
                                $row_duration = (int)$item['units'];
                            }
                            if ($item['units'] == 1 || $item['units'] == '1') {
                                $count++;
                            } else {
                                $count = $count + ((int)$item['units']);
                            }
                        }

                        if ($coupon_deduction > 0) {
                            $total = $total - (((float)$coupon_deduction) / $count);
                            $total = $total * 1.13;
                        } else {
                            if ($affected_by_coupon){
                                $total = $total * ((100 - $coupon_discount) / 100);
                            }
                            $total = $total * 1.13;
                        }
                    }
                    $percent_80 = $total * 0.8;
                    $percent_50 = $total * 0.5;
                    $credit = (float)$customer->getCredit();
                    //1
                    $allow_cancel_time_greater_48 = strtotime( $appointment->getStartDate() ) - ((int) (48 * 60 * 60));
                    if (current_time( 'timestamp' ) < $allow_cancel_time_greater_48 ) {
                        $credit_left = $credit + $percent_80;
                        $customer->setCredit($credit_left);
                        $customer->setNotes($percent_80);
                        $customer->save();
                        $customer->recordCreditTransaction(array($ca->getAppointmentId()), $percent_80, $credit, $credit_left);
                        $ca->cancel();
                        wp_send_json_success(array('message' => 'We have given you credit of $' . $percent_80 . ' for your next purchase'));
                        exit();
                    }

                    //3
                    $allow_cancel_time_more_than_24 = strtotime( $appointment->getStartDate() ) - (int) (24 * 60 * 60);
                    if (current_time( 'timestamp' ) <= $allow_cancel_time_more_than_24) {
                        $credit_left = $credit + $percent_50;
                        $customer->setCredit($credit_left);
                        $customer->setNotes($percent_50);
                        $customer->save();
                        $customer->recordCreditTransaction(array($ca->getAppointmentId()), $percent_50, $credit, $credit_left);
                        $ca->cancel();
                        wp_send_json_success(array('message' => 'We have given you credit of $' . $percent_50 . ' for your next purchase'));
                        exit();
                    }
                }
                // No payment, so just cancel, no credit needed
                else{
                    $allow_cancel_time_more_than_24 = strtotime( $appointment->getStartDate() ) - (int) (24 * 60 * 60);
                    if (current_time( 'timestamp' ) <= $allow_cancel_time_more_than_24) {
                        $ca->cancel();
                        wp_send_json_success(array('message' => 'You have cancelled this appointment'));
                    }
                }
                //ADWAVE END
            }
        }

        wp_send_json_error();
    }
}
