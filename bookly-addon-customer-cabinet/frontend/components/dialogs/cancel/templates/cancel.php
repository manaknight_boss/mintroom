<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
use Bookly\Backend\Components\Controls\Buttons;
?>
<div id="bookly-customer-cabinet-cancel-dialog" class="modal fade" tabindex=-1 role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="modal-title h2"><?php _e( 'Cancel Appointment', 'bookly' ) ?></div>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger collapse" id="bookly-cancel-error"></div>
                    <!-- ADWAVE START -->
                    <p>You are going to cancel a scheduled appointment. </p>
                    <p>Our cancellation policy is the following:</p>
                    <ul>
                        <li>48 hours+ before date of cancellation gives you 80% credit for your next booking</li>
                        <li>Between 24 to 48 hours before date of cancellation gives you 50% credit for your next booking</li>
                        <li>Anything under 24 hours before date of cancellation is not possible</li>
                        <li id="cancel-final-price"></li>
                        <li>Are you sure?</li>
                    </ul>
                    <!-- ADWAVE END -->
                </div>
                <div class="modal-footer">
                    <div>
                        <?php Buttons::renderCustom( 'bookly-yes', 'btn-lg btn-danger', __( 'Yes', 'bookly' ) ) ?>
                        <?php Buttons::renderCustom( null, 'btn-lg btn-default', __( 'No', 'bookly' ), array( 'data-dismiss' => 'modal' ) ) ?>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
