<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
use Bookly\Backend\Components\Controls\Buttons;
?>
<div class="modal fade bookly-js-customer-cabinet-delete-dialog" tabindex=-1 role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="modal-title h2"><?php _e( 'Delete account', 'bookly' ) ?></div>
                </div>
                <div class="modal-body">
                    <div class="bookly-loading"></div>
                    <div class="bookly-js-approve-deleting collapse">
                        <?php _e( 'You are going to delete your account and all information associated with it. Click Confirm to continue or Cancel to cancel the action.', 'bookly' ) ?>
                    </div>
                    <div class="bookly-js-denied-deleting collapse">
                        <?php _e( 'This account cannot be deleted because it is associated with scheduled appointments. Please cancel bookings or contact the service provider.', 'bookly' ) ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <div>
                        <?php Buttons::renderCustom( null, 'btn-lg btn-danger bookly-js-approve-deleting bookly-js-confirm-delete collapse', __( 'Confirm', 'bookly' ) ) ?>
                        <?php Buttons::renderCustom( null, 'btn-lg btn-default bookly-js-approve-deleting', __( 'Cancel', 'bookly' ), array( 'data-dismiss' => 'modal' ) ) ?>
                        <?php Buttons::renderCustom( null, 'btn-lg btn-default bookly-js-denied-deleting', __( 'OK', 'bookly' ), array( 'data-dismiss' => 'modal' ) ) ?>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
