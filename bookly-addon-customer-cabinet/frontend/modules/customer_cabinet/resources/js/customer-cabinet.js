jQuery(function ($) {
    window.booklyCustomerCabinet = function (Options) {
        var $container = $('.' + Options.form_id),
            $tabs = $('.bookly-js-tabs li a', $container),
            sections_ready = [];

        // Appointments section
        function initAppointments($container) {
            var $appointments_table = $('.bookly-appointments-list', $container),
                $reschedule_dialog = $('#bookly-customer-cabinet-reschedule-dialog', $container),
                $reschedule_date = $('#bookly-reschedule-date', $reschedule_dialog),
                $reschedule_time = $('#bookly-reschedule-time', $reschedule_dialog),
                $reschedule_error = $('#bookly-reschedule-error', $reschedule_dialog),
                $reschedule_save = $('#bookly-save', $reschedule_dialog),
                $cancel_dialog = $('#bookly-customer-cabinet-cancel-dialog', $container),
                $cancel_button = $('#bookly-yes', $cancel_dialog),
                appointments_columns = [],
                row;
            Options.appointments_parameters.forEach(function (column) {
                switch (column) {
                    case 'date':
                        appointments_columns.push({
                            data: 'start_date',
                            responsivePriority: 1
                        });
                        break;
                    case 'service':
                        appointments_columns.push({
                            data: 'service_title',
                            responsivePriority: 3
                        });
                        break;
                    case 'staff':
                        appointments_columns.push({
                            data: 'staff_name',
                            responsivePriority: 3
                        });
                        break;
                    case 'status':
                        appointments_columns.push({
                            data: 'status',
                            responsivePriority: 3
                        });
                        break;
                    case 'category':
                        appointments_columns.push({
                            data: 'category',
                            responsivePriority: 4
                        });
                        break;
                    case 'price':
                        appointments_columns.push({
                            data: 'price',
                            responsivePriority: 3
                        });
                        break;
                    case 'cancel':
                        appointments_columns.push({
                            data: 'ca_id',
                            render: function (data, type, row, meta) {
                                switch (row.allow_cancel) {
                                    case 'expired':
                                        // ADWAVE START
                                        //return BooklyCustomerCabinetL10n.expired_appointment;
                                        return "Completed";
                                        //ADWAVE END
                                    case 'blank':
                                        return '';
                                    case 'allow':
                                        return '<button class="btn btn-sm btn-default" data-type="open-modal" data-target=".' + Options.form_id + ' #bookly-customer-cabinet-cancel-dialog" data-cancel-time="' + row.cancel_time + '" data-cancel-price="' + row.cancel_price + '" onclick="cancelModal(this)">' + BooklyCustomerCabinetL10n.cancel + '</button>';
                                    case 'deny':
                                        return BooklyCustomerCabinetL10n.deny_cancel_appointment;
                                }
                            },
                            responsivePriority: 2,
                            orderable: false
                        });
                        break;
                        // case 'reschedule':
                        //ADWAVE START
                        // appointments_columns.push({
                        //     data: 'ca_id',
                        //     render: function (data, type, row, meta) {
                        //         switch (row.allow_reschedule) {
                        //             case 'expired':
                        //                 return BooklyCustomerCabinetL10n.expired_appointment;
                        //             case 'blank':
                        //                 return '';
                        //             case 'allow':
                        //                 return '<button class="btn btn-sm btn-default" data-type="open-modal" data-target=".' + Options.form_id + ' #bookly-customer-cabinet-reschedule-dialog">' + BooklyCustomerCabinetL10n.reschedule + '</button>';
                        //             case 'deny':
                        //                 return BooklyCustomerCabinetL10n.deny_cancel_appointment;
                        //         }
                        //     },
                        //     responsivePriority: 2,
                        //     orderable: false
                        // });
                        //ADWAVE END
                        // break;
                    default:
                        if (column.match("^custom_field")) {
                            appointments_columns.push({
                                data: 'custom_fields.' + column.substring(13),
                                render: $.fn.dataTable.render.text(),
                                responsivePriority: 3,
                                orderable: false
                            });
                        }
                        break;
                }
            });
            /**
             * Init DataTables.
             */
            var appointments_datatable = $appointments_table.DataTable({
                order: [
                    [0, 'desc']
                ],
                info: false,
                lengthChange: false,
                pageLength: 10,
                pagingType: 'numbers',
                searching: false,
                processing: true,
                responsive: true,
                serverSide: true,
                ajax: {
                    url: Options.ajaxurl,
                    type: 'POST',
                    data: function (d) {
                        return $.extend({
                            action: 'bookly_customer_cabinet_get_appointments',
                            form_parameters: Options.appointments_parameters,
                            csrf_token: BooklyCustomerCabinetL10n.csrf_token
                        }, {
                            filter: {}
                        }, d);
                    }
                },
                columns: appointments_columns,
                dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row pull-right'<'col-sm-12 bookly-margin-top-lg'p>>",
                language: {
                    zeroRecords: BooklyCustomerCabinetL10n.zeroRecords,
                    processing: BooklyCustomerCabinetL10n.processing
                }
            });

            $appointments_table.on('click', 'button', function () {
                if ($(this).closest('tr').hasClass('child')) {
                    row = appointments_datatable.row($(this).closest('tr').prev().find('td:first-child'));
                } else {
                    row = appointments_datatable.row($(this).closest('td'));
                }
            });

            // Cancel appointment dialog
            $cancel_button.on('click', function () {
                var ladda = Ladda.create(this);
                ladda.start();
                $.ajax({
                    url: Options.ajaxurl,
                    type: 'POST',
                    data: {
                        action: 'bookly_customer_cabinet_cancel_appointment',
                        csrf_token: BooklyCustomerCabinetL10n.csrf_token,
                        ca_id: row.data().ca_id
                    },
                    dataType: 'json',
                    success: function (response) {
                        ladda.stop();
                        if (response.success) {
                            $cancel_dialog.modal('hide');
                            appointments_datatable.ajax.reload();
                            window.location.reload();
                            // booklyAlert({
                            //     success: [response.data.message]
                            // });
                        } else {
                            $cancel_dialog.modal('hide');
                            booklyAlert({
                                error: [BooklyCustomerCabinetL10n.cancelError]
                            });
                        }
                    }
                });
            });

            // Reschedule appointment dialog
            $reschedule_date.datepicker({
                dateFormat: 'MM dd, yy',
                minDate: BooklyCustomerCabinetL10n.minDate,
                maxDate: BooklyCustomerCabinetL10n.maxDate,
            });
            $reschedule_dialog.on('show.bs.modal', function (e) {
                $reschedule_date.datepicker('setDate', new Date(row.data().date * 1000)).trigger('change');
            });
            $reschedule_date.on('change', function () {
                $reschedule_save.prop('disabled', true);
                $reschedule_time.html('');
                $reschedule_error.hide();
                var dateTime = new Date($reschedule_date.datepicker('getDate'));
                $.ajax({
                    url: Options.ajaxurl,
                    type: 'POST',
                    data: {
                        action: 'bookly_customer_cabinet_get_day_schedule',
                        csrf_token: BooklyCustomerCabinetL10n.csrf_token,
                        ca_id: row.data().ca_id,
                        date: dateTime.getDate() + "-" + (dateTime.getMonth() + 1) + "-" + dateTime.getFullYear()
                    },
                    dataType: 'json',
                    success: function (response) {
                        if (response.data.length) {
                            var time_options = response.data[0].options;
                            $.each(time_options, function (index, option) {
                                var $option = $('<option/>');
                                $option.text(option.title).val(option.value);
                                if (option.disabled) {
                                    $option.attr('disabled', 'disabled');
                                }
                                $reschedule_time.append($option);
                            });
                            $reschedule_save.prop('disabled', false);
                        } else {
                            $reschedule_error.text(BooklyCustomerCabinetL10n.noTimeslots).show();
                        }
                    }
                });
            });
            $reschedule_save.on('click', function (e) {
                e.preventDefault();
                var ladda = Ladda.create(this);
                ladda.start();
                $.ajax({
                    url: Options.ajaxurl,
                    type: 'POST',
                    data: {
                        action: 'bookly_customer_cabinet_save_reschedule',
                        csrf_token: BooklyCustomerCabinetL10n.csrf_token,
                        ca_id: row.data().ca_id,
                        slot: $reschedule_time.val(),
                    },
                    dataType: 'json',
                    success: function (response) {
                        ladda.stop();
                        if (response) {
                            $reschedule_dialog.modal('hide');
                            appointments_datatable.ajax.reload();
                        }
                    }
                });
            });
        }

        // Profile section
        function initProfile($container) {
            var $profile_content = $('.bookly-js-customer-cabinet-content-profile', $container),
                $form = $('form', $profile_content),
                $delete_btn = $('button.bookly-js-delete-profile', $profile_content),
                $delete_modal = $('.bookly-js-customer-cabinet-delete-dialog', $container),
                $delete_loading = $('.bookly-loading', $delete_modal),
                $approve_deleting = $('.bookly-js-approve-deleting', $delete_modal),
                $denied_deleting = $('.bookly-js-denied-deleting', $delete_modal),
                $confirm_delete_btn = $('.bookly-js-confirm-delete', $delete_modal),
                $phone_field = $('.bookly-js-user-phone-input', $profile_content),
                $save_btn = $('button.bookly-js-save-profile', $profile_content);
            if (Options.intlTelInput.enabled) {
                $phone_field.intlTelInput({
                    preferredCountries: [Options.intlTelInput.country],
                    initialCountry: Options.intlTelInput.country,
                    geoIpLookup: function (callback) {
                        $.get('https://ipinfo.io', function () {}, 'jsonp').always(function (resp) {
                            var countryCode = (resp && resp.country) ? resp.country : '';
                            callback(countryCode);
                        });
                    },
                    utilsScript: Options.intlTelInput.utils
                });
            }
            $save_btn.on('click', function (e) {
                e.preventDefault();
                var ladda = Ladda.create(this);
                ladda.start();
                $('.form-group', $profile_content).removeClass('has-error');
                $('.form-group .bookly-js-error').remove();
                var phone_number = $phone_field.val();
                try {
                    phone_number = Options.intlTelInput.enabled ? $phone_field.intlTelInput('getNumber') : $phone_field.val();
                    if (phone_number == '') {
                        phone_number = $phone_field.val();
                    }
                } catch (error) {}
                $phone_field.val(phone_number);

                var data = $form.serializeArray();
                data.push({
                    name: 'action',
                    value: 'bookly_customer_cabinet_save_profile'
                });
                data.push({
                    name: 'csrf_token',
                    value: BooklyCustomerCabinetL10n.csrf_token
                });
                data.push({
                    name: 'columns',
                    value: Options.profile_parameters
                });
                $.ajax({
                    url: Options.ajaxurl,
                    type: 'POST',
                    data: data,
                    dataType: 'json',
                    success: function (response) {
                        if (response.success) {
                            booklyAlert({
                                success: [BooklyCustomerCabinetL10n.profile_update_success]
                            });
                            if ($('[name="current_password"]', $profile_content).val()) {
                                window.location.reload();
                            }
                        } else {
                            $.each(response.errors, function (name, value) {
                                if (name == 'info_fields') {
                                    $.each(value, function (id, text) {
                                        var $form_group = $('.form-group[data-id="customer_information_' + id + '"]', $profile_content);
                                        $form_group.addClass('has-error');
                                        $form_group.append('<div class="bookly-js-error text-danger">' + text + '</div>');
                                    });
                                } else {
                                    var $form_group = $('.form-group [id="bookly_' + name + '"]', $profile_content).closest('.form-group');
                                    $form_group.addClass('has-error');
                                    $form_group.append('<div class="bookly-js-error text-danger">' + value + '</div>');
                                }
                            });
                            $('html, body').animate({
                                scrollTop: $profile_content.find('.form-group.has-error').first().offset().top - 100
                            }, 1000);
                        }
                        ladda.stop();
                    }
                });
            });
            $delete_btn.on('click', function (e) {
                e.preventDefault();
                $approve_deleting.hide();
                $denied_deleting.hide();
                $delete_loading.show();
                $delete_modal.modal('show');
                $.ajax({
                    url: Options.ajaxurl,
                    type: 'POST',
                    data: {
                        action: 'bookly_customer_cabinet_check_future_appointments',
                        csrf_token: BooklyCustomerCabinetL10n.csrf_token,
                    },
                    dataType: 'json',
                    success: function (response) {
                        $delete_loading.hide();
                        if (response.success) {
                            $approve_deleting.show();
                        } else {
                            $denied_deleting.show()
                        }
                    }
                });
            });
            $confirm_delete_btn.on('click', function (e) {
                e.preventDefault();
                var ladda = Ladda.create(this);
                ladda.start();
                $.ajax({
                    url: Options.ajaxurl,
                    type: 'POST',
                    data: {
                        action: 'bookly_customer_cabinet_delete_profile',
                        csrf_token: BooklyCustomerCabinetL10n.csrf_token,
                    },
                    dataType: 'json',
                    success: function (response) {
                        ladda.stop();
                        if (response.success) {
                            $delete_modal.modal('hide');
                            window.location.reload();
                        }
                    }
                });
            });
        }

        function initSection(section) {
            if ($.inArray(section, sections_ready) === -1) {
                switch (section) {
                    case 'appointments':
                        initAppointments($container);
                        sections_ready.push(section);
                        break;
                    case 'profile':
                        initProfile($container);
                        sections_ready.push(section);
                        break;
                }
            }
        }

        if (Options.tabs.length > 1) {
            // Tabs
            $tabs.on('click', function () {
                var section = $(this).attr('href').substring(1);
                $('.bookly-js-customer-cabinet-content', $container).hide();
                $('.bookly-js-customer-cabinet-content-' + section, $container).show();
                initSection(section);
            });
            $tabs.first().trigger('click');
        } else {
            var section = Options.tabs[0];
            $('.bookly-js-customer-cabinet-content', $container).show();
            initSection(section);
        }

        $container.on('click', '[data-type="open-modal"]', function () {
            $($(this).attr('data-target')).modal('show');
        });
    }
});

function cancelModal(obj) {
    var cancel_time = obj.getAttribute('data-cancel-time');
    var cancel_price = obj.getAttribute('data-cancel-price');
    var cancel_final_price = document.getElementById('cancel-final-price');
    cancel_final_price.innerHTML = 'The amount of credit you will recieve is C$' + Number(cancel_price).toFixed(2) + '.';
}