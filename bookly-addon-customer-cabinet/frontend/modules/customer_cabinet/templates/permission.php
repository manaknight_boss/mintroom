<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<?php if ( is_user_logged_in() ) : ?>
    <div id="bookly-tbs">
        <div>
            <?php _e( 'You don\'t have permissions to view this content.', 'bookly' ) ?>
        </div>
    </div>
<?php else : ?>
    <?php wp_login_form() ?>
    <!-- ADWAVE START -->
        <a class="bookly-left bookly-btn-cancel" href="<?php echo esc_url( wp_lostpassword_url() ) ?>" target="_blank"><?php _e( 'Forgot your password?' ) ?></a><br>
        <a class="bookly-left bookly-btn-cancel" href="/register/">Register</a>
    <!-- ADWAVE END -->    
<?php endif ?>