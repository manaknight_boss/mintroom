<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
use Bookly\Lib\Utils\Common;
use Bookly\Lib\Config;
use Bookly\Lib as BooklyLib;
use BooklyPro\Frontend\Components\Fields\Birthday;
use BooklyCustomerCabinet\Frontend\Components\Dialogs;
use Bookly\Lib\Utils\Price;

/** @var \Bookly\Lib\Entities\Customer $customer */

$appointments_parameters = isset( $attributes['appointments'] ) ? explode( ',', $attributes['appointments'] ) : array();
$profile_parameters      = isset( $attributes['profile'] ) ? explode( ',', $attributes['profile'] ) : array();
$tabs                    = array_key_exists( 'tabs', $attributes ) ? explode( ',', $attributes['tabs'] ) : array();

$tabs[] = 'credit_history';

?>
<script type="text/javascript">
    (function (win, fn) {
        var done = false, top = true,
            doc = win.document,
            root = doc.documentElement,
            modern = doc.addEventListener,
            add = modern ? 'addEventListener' : 'attachEvent',
            rem = modern ? 'removeEventListener' : 'detachEvent',
            pre = modern ? '' : 'on',
            init = function (e) {
                if (e.type == 'readystatechange') if (doc.readyState != 'complete') return;
                (e.type == 'load' ? win : doc)[rem](pre + e.type, init, false);
                if (!done) {
                    done = true;
                    fn.call(win, e.type || e);
                }
            },
            poll = function () {
                try {
                    root.doScroll('left');
                } catch (e) {
                    setTimeout(poll, 50);
                    return;
                }
                init('poll');
            };
        if (doc.readyState == 'complete') fn.call(win, 'lazy');
        else {
            if (!modern) if (root.doScroll) {
                try {
                    top = !win.frameElement;
                } catch (e) {
                }
                if (top) poll();
            }
            doc[add](pre + 'DOMContentLoaded', init, false);
            doc[add](pre + 'readystatechange', init, false);
            win[add](pre + 'load', init, false);
        }
    })(window, function () {
        window.booklyCustomerCabinet({
            ajaxurl                 : <?php echo json_encode( admin_url( 'admin-ajax.php' ) ) ?>,
            form_id                 : <?php echo json_encode( $form_id ) ?>,
            tabs                    : <?php echo json_encode( $tabs ) ?>,
            appointments_parameters : <?php echo json_encode( $appointments_parameters ) ?>,
            profile_parameters      : <?php echo json_encode( $profile_parameters ) ?>,
            intlTelInput            : {
                'enabled': <?php echo (int) ( get_option( 'bookly_cst_phone_default_country' ) != 'disabled' ) ?>,
                'utils'  : <?php echo json_encode( is_rtl() ? '' : plugins_url( 'intlTelInput.utils.js', BooklyLib\Plugin::getDirectory() . '/frontend/resources/js/intlTelInput.utils.js' ) ) ?>,
                'country': <?php echo json_encode( get_option( 'bookly_cst_phone_default_country' ) ) ?>
            }
        });
    });
</script>
<div id="bookly-tbs" class="wrap bookly-customer-cabinet <?php echo $form_id ?>">
    <div class="bookly-tbs-body">
        <div class="bookly-fc-inner">
            <div class="bookly-margin-top-xlg">
                <?php if ( count( $tabs ) > 1 ) : ?>
                <?php // Adwave Custom Logout Link ?>
                    <div class='cabinetUsernameLogout'>
                        <a href="/wp-login.php?action=logout" class="logout-link">Logout</a>
                        <?php echo '<div class="cabinetUsername">Hello '.esc_attr( $customer->getFullName() ).'</div>';
                        echo '<br/><span>';
                        if ($customer->getCredit() > 0){
                                echo 'Credit Available: ' . Price::format($customer->getCredit());
                        }
                        echo '&nbsp;</span>';
                        ?> 
                    </div>
                <?php // End Adwave Custom Logout Link ?>
                    <ul class="bookly-js-tabs nav nav-tabs nav-justified bookly-nav-justified">
                        <?php foreach ( $tabs as $num => $tab ) : ?>
                            <?php switch ( $tab ) :
                                case 'appointments': ?>
                                    <li<?php if ( ! $num ) : ?> class="active"<?php endif ?>>
                                        <a id="bookly-cabinet-appointments-tab" href="#appointments" data-toggle="tab">
                                            <i class="bookly-icon bookly-icon-schedule"></i>
                                            <span class="bookly-nav-tabs-title"><?php _e( 'Appointments', 'bookly' ) ?></span>
                                        </a>
                                    </li>
                                    <?php break; ?>
                                <?php // ADWAVE START ?>
                                <?php case 'credit_history': ?>
                                    <li<?php if ( ! $num ) : ?> class="active"<?php endif ?>>
                                        <a id="bookly-cabinet-appointments-tab" href="#credit_history" data-toggle="tab">
                                            <i class="bookly-icon bookly-icon-draghandle"></i>
                                            <span class="bookly-nav-tabs-title"><?php _e( 'Credit History', 'bookly' ) ?></span>
                                        </a>
                                    </li>
                                    <?php break; ?>
                                <?php // ADWAVE END ?>
                                <?php case 'profile': ?>
                                    <li<?php if ( ! $num ) : ?> class="active"<?php endif ?>>
                                        <a id="bookly-cabinet-profile-tab" href="#profile" data-toggle="tab">
                                            <i class="dashicons dashicons-admin-users"></i>
                                            <span class="bookly-nav-tabs-title"><?php _e( 'Profile', 'bookly' ) ?></span>
                                        </a>
                                    </li>
                                    <?php break; ?>
                                <?php endswitch ?>
                        <?php endforeach ?>
                    </ul>
                <?php endif ?>

                <?php foreach ( $tabs as $num => $tab ) : ?>
                    <?php switch ( $tab ) :
                        case 'appointments': ?>
                            <?php
                            //ADWAVE START
                            
                            if (($key = array_search('reschedule', $appointments_parameters)) !== false) {
                                unset($appointments_parameters[$key]);
                            }
                            //ADWAVE END
                            ?>
                            <div class="bookly-js-customer-cabinet-content bookly-js-customer-cabinet-content-appointments collapse">
                                <table class="table table-striped bookly-appointments-list" width="100%">
                                    <thead>
                                    <tr>
                                        <?php foreach ( $appointments_parameters as $column ) : ?>
                                            <?php if ( $column != 'timezone' )  : ?>
                                                <th><?php echo $titles[ $column ] ?></th>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <?php break; ?>
                        <?php case 'credit_history': ?>
                            <?php // Adwave Credit History ?>
                            <div class="bookly-js-customer-cabinet-content bookly-js-customer-cabinet-content-credit_history collapse">
                                <h3>Credit History</h3>
                                <table class="table table-striped" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Action</th>
                                        <th>Booking IDs</th>
                                        <th>Before</th>
                                        <th>Change</th>
                                        <th>After</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $creditHistory = $customer->getCreditHistory();
                                            foreach ($creditHistory as $creditHistoryEntry) {
                                                $dateObject = new DateTime($creditHistoryEntry['credit_datetime']);
                                                $dateStringer = $dateObject->format('F j, Y g:i a');
                                                echo'
                                                <tr>
                                                    <td>'.$dateStringer.'</td>
                                                    <td>'.($creditHistoryEntry['credit_amount'] < 0 ? "spent":"received").'</td>
                                                    <td>'.$creditHistoryEntry['appointment_id'].'</td>
                                                    <td>$ '.$creditHistoryEntry['credit_before'].'</td>
                                                    <td>$ '.$creditHistoryEntry['credit_amount'].'</td>
                                                    <td>$ '.$creditHistoryEntry['credit_after'].'</td>
                                                    
                                                </tr>
                                                ';
                                            }
                                        ?>
                                    </tbody>
                                </table>
                                </div>
                                <?php // End Adwave Credit History ?>
                            <?php break; ?>
                        <?php case 'profile': ?>
                            <div class="bookly-js-customer-cabinet-content bookly-js-customer-cabinet-content-profile collapse">
                                <form>
                                    <?php foreach ( $profile_parameters as $column ) : ?>
                                        <?php switch ( $column ) :
                                            case 'name': ?>
                                                <?php if ( Config::showFirstLastName() ) : ?>
                                                    <div class="row">
                                                        <div class="form-group col-sm-6">
                                                            <label for="bookly_first_name"><?php echo Common::getTranslatedOption( 'bookly_l10n_label_first_name ' ) ?></label>
                                                            <input type="text" name="first_name" class="form-control" id="bookly_first_name" value="<?php echo esc_attr( $customer->getFirstName() ) ?>"/>
                                                        </div>
                                                        <div class="form-group col-sm-6">
                                                            <label for="bookly_last_name"><?php echo Common::getTranslatedOption( 'bookly_l10n_label_last_name' ) ?></label>
                                                            <input type="text" name="last_name" class="form-control" id="bookly_last_name" value="<?php echo esc_attr( $customer->getLastName() ) ?>"/>
                                                        </div>
                                                    </div>
                                                <?php else : ?>
                                                    <div class="form-group">
                                                        <label for="bookly_full_name"><?php echo Common::getTranslatedOption( 'bookly_l10n_label_name' ) ?></label>
                                                        <input type="text" name="full_name" class="form-control" id="bookly_full_name" value="<?php echo esc_attr( $customer->getFullName() ) ?>"/>
                                                    </div>
                                                <?php endif ?>
                                                <?php break ?>
                                            <?php case 'email': ?>
                                                <div class="form-group">
                                                    <label for="bookly_email"><?php echo Common::getTranslatedOption( 'bookly_l10n_label_email' ) ?></label>
                                                    <input type="text" name="email" class="form-control" id="bookly_email" value="<?php echo esc_attr( $customer->getEmail() ) ?>"/>
                                                </div>
                                                <?php break ?>
                                            <?php case 'phone': ?>
                                                <div class="form-group">
                                                    <label for="bookly_phone"><?php echo Common::getTranslatedOption( 'bookly_l10n_label_phone' ) ?></label>
                                                    <input type="text" name="phone" class="form-control bookly-js-user-phone-input<?php if ( get_option( 'bookly_cst_phone_default_country' ) != 'disabled' ) : ?> bookly-user-phone<?php endif ?>" id="bookly_phone" value="<?php echo esc_attr( $customer->getPhone() ) ?>"/>
                                                </div>
                                                <?php break ?>
                                            <?php case 'birthday': ?>
                                                <div class="row">
                                                    <?php Birthday::renderBootstrap( $customer->getBirthday() ) ?>
                                                </div>
                                                <?php break ?>
                                            <?php case 'address':
                                                $address_show_fields = (array) get_option( 'bookly_cst_address_show_fields', array() );
                                                foreach ( $address_show_fields as $field_name => $field ) : ?>
                                                    <?php if ( $field['show'] ) : ?>
                                                        <div class="form-group">
                                                            <label for="bookly_<?php echo $field_name; ?>"><?php esc_html_e( get_option( 'bookly_l10n_label_' . $field_name ), 'bookly' ) ?></label>
                                                            <input class="form-control" type="text" name=<?php echo $field_name; ?> id="bookly_<?php echo $field_name; ?>" value="<?php echo esc_attr( isset( $customer_address[ $field_name ] ) ? $customer_address[ $field_name ] : '' ) ?>"/>
                                                        </div>
                                                    <?php endif ?>
                                                <?php endforeach ?>
                                                <?php break ?>
                                            <?php case 'wp_password': ?>
                                                <div class="row">
                                                    <div class="form-group col-sm-3">
                                                    <?php // Adwave Custom ?>
                                                        <label for="bookly-wp-user"><?php esc_html_e( 'User Name', 'bookly' ) ?></label>
                                                        <p><?php $user_data = get_userdata( $customer->getWpUserId() ); echo $user_data->user_email ?></p>
                                                    <?php // Adwave Custom ?>
                                                    </div>
                                                    <div class="form-group col-sm-3">
                                                        <label for="bookly_current_password"><?php esc_html_e( 'Current password', 'bookly' ) ?></label>
                                                        <input type="password" name="current_password" class="form-control" id="bookly_current_password" value=""/>
                                                    </div>
                                                    <div class="form-group col-sm-3">
                                                        <label for="bookly_new_password_1"><?php esc_html_e( 'New password', 'bookly' ) ?></label>
                                                        <input type="password" name="new_password_1" class="form-control" id="bookly_new_password_1" value=""/>
                                                    </div>
                                                    <div class="form-group col-sm-3">
                                                        <label for="bookly_new_password_2"><?php esc_html_e( 'Confirm password', 'bookly' ) ?></label>
                                                        <input type="password" name="new_password_2" class="form-control" id="bookly_new_password_2" value=""/>
                                                    </div>
                                                </div>
                                                <?php break ?>
                                            <?php default : ?>
                                                <?php
                                                if ( strpos( $column, 'customer_information' ) === 0 ) {
                                                    BooklyLib\Proxy\CustomerInformation::renderCustomerCabinet( substr( $column, 21 ), $customer );
                                                }
                                                ?>
                                                <?php break; ?>
                                            <?php endswitch ?>
                                    <?php endforeach ?>
                                    <div>
                                        <?php if ( in_array( 'delete', $profile_parameters ) ) : ?>
                                            <button class="btn btn-danger bookly-js-delete-profile" data-type="open-modal" data-target=".<?php echo $form_id ?>.bookly-js-customer-cabinet-delete-dialog"><?php esc_html_e( 'Delete account', 'bookly' ) ?></button>
                                        <?php endif ?>
                                        <button class="btn btn-success pull-right bookly-js-save-profile ladda-button" data-style="zoom-in"><?php esc_html_e( 'Save', 'bookly' ) ?></button>
                                    </div>
                                </form>
                            </div>
                            <?php break ?>
                        <?php endswitch ?>
                <?php endforeach ?>
            </div>
        </div>
    </div>
    <?php Dialogs\Reschedule\Dialog::render() ?>
    <?php Dialogs\Cancel\Dialog::render() ?>
    <?php Dialogs\Delete\Dialog::render() ?>
</div>