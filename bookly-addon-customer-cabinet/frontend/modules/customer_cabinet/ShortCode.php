<?php
namespace BooklyCustomerCabinet\Frontend\Modules\CustomerCabinet;

use BooklyCustomerCabinet\Lib;
use Bookly\Lib as BooklyLib;

/**
 * Class Controller
 * @package BooklyCustomerCabinet\Frontend\Modules\CustomerCabinet
 */
class ShortCode extends BooklyLib\Base\Component
{
    /**
     * Init component.
     */
    public static function init()
    {
        // Register shortcodes.
        add_shortcode( 'bookly-customer-cabinet', array( __CLASS__, 'render' ) );

        // Assets.
        if ( get_option( 'bookly_gen_link_assets_method' ) == 'enqueue' ) {
            add_action( 'wp_enqueue_scripts', array( __CLASS__, 'linkStyles' ) );
            add_action( 'wp_enqueue_scripts', array( __CLASS__, 'linkScripts' ) );
        } else {
            add_action( 'wp_print_styles', array( __CLASS__, 'linkStyles' ) );
            add_action( 'wp_print_scripts', array( __CLASS__, 'linkScripts' ) );
        }
    }

    /**
     * Link styles.
     */
    public static function linkStyles()
    {
        if (
            get_option( 'bookly_gen_link_assets_method' ) == 'enqueue' ||
            BooklyLib\Utils\Common::postsHaveShortCode( 'bookly-customer-cabinet' )
        ) {
            $bookly_ver = BooklyLib\Plugin::getVersion();
            $bookly_url = plugins_url( '', BooklyLib\Plugin::getMainFile() );
            $cabinet    = plugins_url( '', Lib\Plugin::getMainFile() );

            wp_enqueue_style( 'bookly-ladda-min', $bookly_url . '/frontend/resources/css/ladda.min.css', array(), $bookly_ver );
            if ( get_option( 'bookly_cst_phone_default_country' ) != 'disabled' ) {
                wp_enqueue_style( 'bookly-intlTelInput', $bookly_url . '/frontend/resources/css/intlTelInput.css', array(), $bookly_ver );
            }
            wp_enqueue_style( 'bookly-bootstrap-theme', $bookly_url . '/backend/resources/bootstrap/css/bootstrap-theme.min.css', array( 'dashicons' ), $bookly_ver );
            wp_enqueue_style( 'bookly-jquery-ui', $bookly_url . '/backend/resources/css/jquery-ui-theme/jquery-ui.min.css', array( 'bookly-bootstrap-theme' ), $bookly_ver );
            wp_enqueue_style( 'bookly-customer-cabinet', $cabinet . '/frontend/modules/customer_cabinet/resources/css/customer-cabinet.css', array( 'bookly-jquery-ui' ), Lib\Plugin::getVersion() );
        }
    }

    /**
     * Link scripts.
     */
    public static function linkScripts()
    {
        if (
            get_option( 'bookly_gen_link_assets_method' ) == 'enqueue' ||
            BooklyLib\Utils\Common::postsHaveShortCode( 'bookly-customer-cabinet' )
        ) {
            $bookly_ver = BooklyLib\Plugin::getVersion();
            $bookly_url = plugins_url( '', BooklyLib\Plugin::getMainFile() );
            $cabinet    = plugins_url( '', Lib\Plugin::getMainFile() );

            wp_enqueue_script( 'bookly-spin', $bookly_url . '/frontend/resources/js/spin.min.js', array(), $bookly_ver );
            wp_enqueue_script( 'bookly-ladda', $bookly_url . '/frontend/resources/js/ladda.min.js', array( 'bookly-spin' ), $bookly_ver );
            if ( get_option( 'bookly_cst_phone_default_country' ) != 'disabled' ) {
                wp_enqueue_script( 'bookly-intlTelInput', $bookly_url . '/frontend/resources/js/intlTelInput.min.js', array( 'jquery' ), $bookly_ver );
            }
            wp_enqueue_script( 'bookly-bootstrap', $bookly_url . '/backend/resources/bootstrap/js/bootstrap.min.js', array(), $bookly_ver );
            wp_enqueue_script( 'bookly-alert', $bookly_url . '/backend/resources/js/alert.js', array(), $bookly_ver );
            wp_enqueue_script( 'bookly-datatables', $bookly_url . '/backend/resources/js/datatables.min.js', array( 'jquery' ), $bookly_ver );
            wp_enqueue_script( 'bookly-customer-cabinet', $cabinet . '/frontend/modules/customer_cabinet/resources/js/customer-cabinet.js', array( 'jquery', 'jquery-ui-datepicker' ), Lib\Plugin::getVersion() );

            wp_localize_script( 'bookly-customer-cabinet', 'BooklyCustomerCabinetL10n', array(
                'csrf_token'              => BooklyLib\Utils\Common::getCsrfToken(),
                'zeroRecords'             => __( 'No appointments.', 'bookly' ),
                'minDate'                 => 0,
                'maxDate'                 => BooklyLib\Config::getMaximumAvailableDaysForBooking(),
                'expired_appointment'     => __( 'Expired', 'bookly' ),
                'deny_cancel_appointment' => __( 'Not allowed', 'bookly' ),
                'cancel'                  => __( 'Cancel', 'bookly' ),
                'reschedule'              => __( 'Reschedule', 'bookly' ),
                'noTimeslots'             => __( 'There are no time slots for selected date.', 'bookly' ),
                'cancelError'             => __( 'Unfortunately, you\'re not able to cancel the appointment because the required time limit prior to canceling has expired.', 'bookly' ),
                'profile_update_success'  => __( 'Profile updated successfully.', 'bookly' ),
                'processing'              => __( 'Processing...', 'bookly' ),
            ) );
        }
    }

    /**
     * Render Customer Services shortcode.
     *
     * @param array $attributes
     * @return string
     */
    public static function render( $attributes )
    {
        // Disable caching.
        BooklyLib\Utils\Common::noCache();

        $customer = new BooklyLib\Entities\Customer();
        if ( is_user_logged_in() && $customer->loadBy( array( 'wp_user_id' => get_current_user_id() ) ) ) {
            $titles = array(
                'category'   => BooklyLib\Utils\Common::getTranslatedOption( 'bookly_l10n_label_category' ),
                'service'    => BooklyLib\Utils\Common::getTranslatedOption( 'bookly_l10n_label_service' ),
                'staff'      => BooklyLib\Utils\Common::getTranslatedOption( 'bookly_l10n_label_employee' ),
                'date'       => __( 'Date', 'bookly' ),
                'time'       => __( 'Time', 'bookly' ),
                'price'      => __( 'Price', 'bookly' ),
                'cancel'     => __( 'Cancel', 'bookly' ),
                'reschedule' => __( 'Reschedule', 'bookly' ),
                'status'     => __( 'Status', 'bookly' ),
            );
            if ( BooklyLib\Config::customFieldsActive() && get_option( 'bookly_custom_fields_enabled' ) ) {
                //ADWAVE START
                foreach ( (array) @BooklyLib\Proxy\CustomFields::getTranslated() as $field ) {
                //ADWAVE END
                    if ( ! in_array( $field->type, array( 'captcha', 'text-content', 'file' ) ) ) {
                        $titles[ 'custom_field_' . $field->id ] = $field->label;
                    }
                }
            }

            $customer_address = array(
                'country'            => $customer->getCountry(),
                'state'              => $customer->getState(),
                'postcode'           => $customer->getPostcode(),
                'city'               => $customer->getCity(),
                'street'             => $customer->getStreet(),
                'additional_address' => $customer->getAdditionalAddress(),
                //ADWAVE START
                'credit' => $customer->getCredit()
                //ADWAVE END
            );
            return self::renderTemplate( 'short_code', array( 'form_id' => uniqid( 'bookly-js-customer-cabinet-' ), 'customer' => $customer, 'customer_address' => $customer_address, 'attributes' => (array) $attributes, 'titles' => $titles ), false );
        }

        return self::renderTemplate( 'permission', array(), false );
    }
}