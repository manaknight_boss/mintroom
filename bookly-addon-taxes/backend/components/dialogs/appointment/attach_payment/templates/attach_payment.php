<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<div class="form-group bookly-margin-left-xlg"<?php if ( get_option( 'bookly_taxes_in_price' ) == 'included' ) : ?> ng-class="{'has-error': (form.attach.payment_price * 1) < (form.attach.payment_tax * 1)}"<?php endif ?>>
    <label for="bookly-attach-payment-tax"><?php _e( 'Tax', 'bookly' ) ?></label>
    <input id="bookly-attach-payment-tax" class="form-control bookly-js-attach-payment-tax" type="text" ng-model="form.attach.payment_tax"/>
</div>

