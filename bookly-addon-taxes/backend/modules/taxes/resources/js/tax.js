jQuery(function($) {

    var
        $taxes_list       = $('#bookly-tax'),
        $check_all_button = $('#bookly-tax-check-all'),
        $modal            = $('#bookly-tax-modal'),
        $modal_title      = $('#bookly-tax-modal-title'),
        $tax_title        = $('#bookly-tax-title'),
        $tax_rate         = $('#bookly-tax-rate'),
        $save_button      = $('#bookly-tax-save'),
        $delete_button    = $('#bookly-delete'),
        $add_button       = $('#bookly-tax-add'),
        row
        ;

    /**
     * Init DataTables.
     */
    var dt = $taxes_list.DataTable({
        paging: false,
        info: false,
        searching: false,
        processing: true,
        responsive: true,
        ajax: {
            url: ajaxurl,
            data: { action: 'bookly_taxes_get_taxes', csrf_token : BooklyTaxesL10n.csrfToken }
        },
        order: [0, 'asc'],
        columns: [
            {data: 'title'},
            {data: 'rate'},
            {
                responsivePriority: 1,
                orderable: false,
                className: "text-right",
                render: function (data, type, row, meta) {
                    return '<button type="button" class="btn btn-default"><i class="glyphicon glyphicon-edit"></i> ' + BooklyTaxesL10n.edit + '</a>';
                }
            },
            {
                responsivePriority: 1,
                orderable: false,
                render: function (data, type, row, meta) {
                    return '<input type="checkbox" class="bookly-js-delete" value="' + row.id + '" />';
                }
            }
        ],
        language: {
            zeroRecords: BooklyTaxesL10n.zeroRecords,
            processing:  BooklyTaxesL10n.processing
        }
    });

    /**
     * Select all taxes.
     */
    $check_all_button.on('change', function () {
        $taxes_list.find('tbody input:checkbox').prop('checked', this.checked);
    });

    /**
     * On tax select.
     */
    $taxes_list.on('change', 'tbody input:checkbox', function () {
        $check_all_button.prop('checked', $taxes_list.find('tbody input:not(:checked)').length == 0);
    });

    /**
     * Edit tax.
     */
    $taxes_list.on('click', 'button', function () {
        row = dt.row($(this).closest('td'));
        $modal.modal('show');
    });

    /**
     * Add new tax.
     */
    $add_button.on('click', function () {
        row = null;
        $modal.modal('show');
    });

    /**
     * On show modal.
     */
    $modal.on('show.bs.modal', function (e) {
        var data;
        if (row) {
            data = row.data();
            $modal_title.html(BooklyTaxesL10n.title.edit);
        } else {
            data = {title: '', rate: ''};
            $modal_title.html(BooklyTaxesL10n.title.new);
            $modal_title.show();
        }
        $tax_title.val(data.title);
        $tax_rate.val(data.rate);
    });

    /**
     * Save tax.
     */
    $save_button.on('click', function (e) {
        e.preventDefault();
        var $form = $(this).parents('form'),
            data  = $form.serializeArray(),
            ladda = Ladda.create(this, {timeout: 2000}),
            abort = false;

        $('input[required]', $form).each(function () {
            if ($(this).val() == '') {
                $(this).closest('.form-group').addClass('has-error');
                abort = true;
            } else {
                $(this).closest('.form-group').removeClass('has-error');
            }
        });
        if (abort) {
            return false;
        }

        data.push({name: 'action', value: 'bookly_taxes_save_tax'});
        if (row){
            data.push({name: 'id', value: row.data().id});
        }
        ladda.start();
        $.ajax({
            url  : ajaxurl,
            type : 'POST',
            data : data,
            dataType : 'json',
            success  : function(response) {
                if (response.success) {
                    if (row) {
                        row.data(response.data).draw();
                    } else {
                        dt.row.add(response.data).draw();
                    }
                    $modal.modal('hide');
                } else {
                    alert(response.data.message);
                }
                ladda.stop();
            }
        });
    });

    /**
     * Delete taxes.
     */
    $delete_button.on('click', function () {
        if (confirm(BooklyTaxesL10n.are_you_sure)) {
            var ladda = Ladda.create(this),
                data  = [],
                $checkboxes = $taxes_list.find('input.bookly-js-delete:checked');
            ladda.start();

            $checkboxes.each(function () {
                data.push(this.value);
            });

            $.ajax({
                url  : ajaxurl,
                type : 'POST',
                data : {
                    action     : 'bookly_taxes_delete_taxes',
                    csrf_token : BooklyTaxesL10n.csrfToken,
                    taxes      : data
                },
                dataType : 'json',
                success  : function(response) {
                    ladda.stop();
                    if (response.success) {
                        dt.rows($checkboxes.closest('td')).remove().draw();
                    } else {
                        alert(response.data.message);
                    }
                }
            });
        }
    });
});