<?php
namespace BooklyTaxes\Backend\Modules\Taxes;

use Bookly\Lib as BooklyLib;

/**
 * Class Page
 * @package BooklyTaxes\Backend\Modules\Taxes
 */
class Page extends BooklyLib\Base\Component
{
    /**
     * Render page.
     */
    public static function render()
    {
        self::enqueueStyles( array(
            'bookly' => array(
                'backend/resources/bootstrap/css/bootstrap-theme.min.css',
                'frontend/resources/css/ladda.min.css',
            ),
        ) );

        self::enqueueScripts( array(
            'bookly' => array(
                'backend/resources/bootstrap/js/bootstrap.min.js' => array( 'jquery' ),
                'backend/resources/js/datatables.min.js'  => array( 'jquery' ),
                'frontend/resources/js/spin.min.js' => array( 'jquery' ),
                'frontend/resources/js/ladda.min.js' => array( 'jquery' ),
            ),
            'module' => array( 'js/tax.js' => array( 'jquery' ), ),
        ) );

        wp_localize_script( 'bookly-tax.js', 'BooklyTaxesL10n', array(
            'csrfToken'    => BooklyLib\Utils\Common::getCsrfToken(),
            'edit'         => __( 'Edit', 'bookly' ),
            'title'        => array(
                'new'  => __( 'New tax', 'bookly' ),
                'edit' => __( 'Edit tax', 'bookly' ),
            ),
            'are_you_sure' => __( 'Are you sure?', 'bookly' ),
            'zeroRecords'  => __( 'No taxes found.', 'bookly' ),
            'processing'   => __( 'Processing...', 'bookly' ),
        ) );

        self::renderTemplate( 'index' );
    }
}