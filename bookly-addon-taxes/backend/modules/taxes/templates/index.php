<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
use Bookly\Backend\Components as BooklyComponents;
use BooklyTaxes\Backend\Components;
?>
<div id="bookly-tbs" class="wrap">
    <div class="bookly-tbs-body">
        <div class="page-header text-right clearfix">
            <div class="bookly-page-title">
                <?php _e( 'Taxes', 'bookly' ) ?>
            </div>
            <?php BooklyComponents\Support\Buttons::render( $self::pageSlug() ) ?>
        </div>

        <div class="panel panel-default bookly-main">
            <div class="panel-body">
                <div class="form-inline bookly-margin-bottom-lg text-right">
                    <div class="form-group">
                        <button type="button" id="bookly-tax-add" class="btn btn-success">
                            <i class="glyphicon glyphicon-plus"></i> <?php _e( 'Add Tax', 'bookly' ) ?>
                        </button>
                    </div>
                </div>
                <table class="table table-striped" id="bookly-tax" width="100%">
                    <thead>
                        <tr>
                            <th><?php _e( 'Title', 'bookly' ) ?></th>
                            <th><?php _e( 'Rate', 'bookly' ) ?></th>
                            <th></th>
                            <th width="16"><input type="checkbox" id="bookly-tax-check-all" /></th>
                        </tr>
                    </thead>
                </table>
                <div class="text-right">
                    <?php BooklyComponents\Controls\Buttons::renderDelete() ?>
                </div>
            </div>
        </div>
    </div>

    <?php Components\Dialogs\Tax\Edit::render() ?>
</div>