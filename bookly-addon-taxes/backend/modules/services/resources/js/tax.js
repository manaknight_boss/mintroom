jQuery(function ($) {
    $(document.body)
        .on('service.initForm', {},
            // Bind an event handler to the components for service panel.
            function (event, $panel) {
                var $taxation = $panel.find('.bookly-js-taxation'),
                    serviceId = $panel.data('service-id')
                ;

                // Taxation drop-down.
                $taxation.booklyDropdown({
                    inputsName: 'taxes[]',
                    options: $.map(BooklyTaxesL10n.taxes, function (tax) {
                        tax.selected = BooklyTaxesL10n.serviceTaxes[serviceId] && $.inArray(tax.value, BooklyTaxesL10n.serviceTaxes[serviceId]) > -1;
                        return tax;
                    })
                });
            }
        )
        .on('service.resetForm', {},
            function (event, $panel) {
                $panel.find('.bookly-js-taxation').booklyDropdown('reset');
            }
        );
});