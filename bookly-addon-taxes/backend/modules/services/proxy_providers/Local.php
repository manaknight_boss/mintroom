<?php
namespace BooklyTaxes\Backend\Modules\Services\ProxyProviders;

use Bookly\Backend\Modules\Services\Proxy;

/**
 * Class Local
 * @package BooklyTaxes\Backend\Modules\Services\ProxyProviders
 */
class Local extends Proxy\Taxes
{
    /**
     * @inheritdoc
     */
    public static function renderSubForm( array $service )
    {
        self::renderTemplate( 'sub_form', compact( 'service' ) );
    }
}