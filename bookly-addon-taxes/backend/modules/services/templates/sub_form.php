<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<div class="bookly-js-service bookly-js-service-simple bookly-js-service-collaborative bookly-js-service-compound">
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label><?php _e( 'Taxation', 'bookly' ) ?></label><br/>
                <ul class="bookly-js-taxation"
                    data-icon-class="dashicons dashicons-chart-pie"
                    data-txt-select-all="<?php esc_attr_e( 'All', 'bookly' ) ?>"
                    data-txt-all-selected="<?php esc_attr_e( 'All', 'bookly' ) ?>"
                    data-txt-nothing-selected="<?php esc_attr_e( 'Nothing selected', 'bookly' ) ?>"
                >
                </ul>
            </div>
        </div>
    </div>
</div>