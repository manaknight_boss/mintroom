<?php
namespace BooklyStripe\Frontend\Modules\Stripe;

use BooklyStripe\Lib;
use Bookly\Lib as BooklyLib;
use Bookly\Frontend\Modules\Booking\Lib\Errors;
use BooklyStripe\Lib\Payment\Lib\Stripe as StripeApi;
use Bookly\Lib\Entities\Customer;
/**
 * Class Controller
 * @package Bookly\Frontend\Modules\Stripe
 */
class Ajax extends BooklyLib\Base\Ajax
{
    /**
     * @inheritdoc
     */
    protected static function permissions()
    {
        return array( '_default' => 'anonymous' );
    }

    /**
     * Do payment.
     */
    public static function payment()
    {
        $response = null;
        $userData = new BooklyLib\UserBookingData( self::parameter( 'form_id' ) );

        if ( $userData->load() ) {
            $failed_cart_key = $userData->cart->getFailedKey();
            if ( $failed_cart_key === null ) {
                include_once Lib\Plugin::getDirectory() . '/lib/payment/Stripe/init.php';
                StripeApi\Stripe::setApiKey( get_option( 'bookly_stripe_secret_key' ) );
                StripeApi\Stripe::setApiVersion( '2015-08-19' );

                $cart_info = $userData->cart->getInfo( BooklyLib\Entities\Payment::TYPE_STRIPE );
                try {
                    $stripe_amount = $cart_info->getGatewayAmount();

                    if ( in_array( get_option( 'bookly_pmt_currency' ), array( 'BIF', 'CLP', 'DJF', 'GNF', 'JPY', 'KMF', 'KRW', 'MGA', 'PYG', 'RWF', 'VND', 'VUV', 'XAF', 'XOF', 'XPF', ) ) ) {
                        // Zero-decimal currency
                    } else {
                        $stripe_amount = $stripe_amount * 100; // amount in cents
                    }
                    $charge = StripeApi\Charge::create( array(
                        'amount'      => (int) $stripe_amount,
                        'currency'    => get_option( 'bookly_pmt_currency' ),
                        'source'      => self::parameter( 'card' ), // contain token or card data
                        'description' => 'Charge for ' . $userData->getEmail(),
                    ) );

                    if ( $charge->paid ) {
                        $coupon = $userData->getCoupon();
                        if ( $coupon ) {
                            $coupon->claim();
                            $coupon->save();
                        }
                        $payment = new BooklyLib\Entities\Payment();
                        $payment
                            ->setType( BooklyLib\Entities\Payment::TYPE_STRIPE )
                            ->setStatus( BooklyLib\Entities\Payment::STATUS_COMPLETED )
                            ->setCartInfo( $cart_info )
                            ->save();
                        $order = $userData->save( $payment );
                        $payment->setDetailsFromOrder( $order, $cart_info )->save();
                        BooklyLib\Notifications\Cart\Sender::send( $order );
                        $response = array( 'success' => true );
                    } else {
                        $response = array( 'success' => false, 'error' => Errors::PAYMENT_ERROR, 'error_message' => __( 'Error', 'bookly' ) );
                    }
                } catch ( \Exception $e ) {
                    $response = array( 'success' => false, 'error' => Errors::PAYMENT_ERROR, 'error_message' => $e->getMessage() );
                }
            } else {
                $response = array(
                    'success'         => false,
                    'error'           => Errors::CART_ITEM_NOT_AVAILABLE,
                    'failed_cart_key' => $failed_cart_key,
                );
            }
        } else {
            $response = array( 'success' => false, 'error' => Errors::SESSION_ERROR );
        }

        // Output JSON response.
        wp_send_json( $response );
    }

    //ADWAVE START
    /**
     * Do payment.
     */
    public static function creditPayment()
    {
        $response = null;
        $userData = new BooklyLib\UserBookingData( self::parameter( 'form_id' ) );

        if ( $userData->load() ) {
            $failed_cart_key = $userData->cart->getFailedKey();
            if ( $failed_cart_key === null ) {
                include_once Lib\Plugin::getDirectory() . '/lib/payment/Stripe/init.php';
                StripeApi\Stripe::setApiKey( get_option( 'bookly_stripe_secret_key' ) );
                StripeApi\Stripe::setApiVersion( '2015-08-19' );

                $cart_info = $userData->cart->getInfo( BooklyLib\Entities\Payment::TYPE_STRIPE );
                try {
                    $stripe_amount = $cart_info->getGatewayAmount();
                    $tax = $cart_info->getTotalTax();
                    $credit_left = 0;
                    $total_minus_credit = $cart_info->getTotal();
                    if (is_user_logged_in()) {
                        $customer    = Customer::query()->where( 'wp_user_id', get_current_user_id() )->findOne();
                        $credit = (float)$customer->getCredit();
                        if ($total_minus_credit - $credit > 0) {
                            $stripe_amount = $total_minus_credit - $credit;
                            $total_minus_credit = $total_minus_credit - $credit;
                        } else {
                            $credit_left = $credit - $total_minus_credit;
                            $stripe_amount = 0;
                            $total_minus_credit = 0;
                        }
                    }

                    $stripe_amount = $total_minus_credit * 100; // amount in cents

                    $charge = StripeApi\Charge::create( array(
                        'amount'      => (int) $stripe_amount,
                        'currency'    => get_option( 'bookly_pmt_currency' ),
                        'source'      => self::parameter( 'card' ), // contain token or card data
                        'description' => 'Charge for ' . $userData->getEmail(),
                    ) );

                    if ( $charge->paid ) {
                        $coupon = $userData->getCoupon();
                        if ( $coupon ) {
                            $coupon->claim();
                            $coupon->save();
                        }
                        $payment = new BooklyLib\Entities\Payment();
                        $payment
                            ->setType( BooklyLib\Entities\Payment::TYPE_STRIPE_CREDIT )
                            ->setStatus( BooklyLib\Entities\Payment::STATUS_COMPLETED )
                            ->setCartInfo( $cart_info )
                            ->setGatewayPriceCorrection( $credit )
                            ->setTotal( $total_minus_credit )
                            ->setPaid( $total_minus_credit )
                            ->setTax( $tax )
                            ->save();
                        $order = $userData->save( $payment );
                        $payment->setDetailsFromOrder( $order, $cart_info )->save();
                        BooklyLib\Notifications\Cart\Sender::send( $order );
                        $customer    = Customer::query()->where( 'wp_user_id', get_current_user_id() )->findOne();
                        // ADWAVE START
                        $customer->setCredit($credit_left);
                        
                            // Record Credit
                            $creditIdsToRecord = array();
                            foreach ( $order->getItems() as $item ) {
                                $creditIdsToRecord[] = $item->getCA()->getAppointmentId();
                            }
                            $customer->recordCreditTransaction($creditIdsToRecord, (-1*($cart_info->getTotal() - $total_minus_credit)), $credit, $credit_left);

                        // ADWAVE END
                        $customer->save();
                        $response = array( 'success' => true );
                    } else {
                        $response = array( 'success' => false, 'error' => Errors::PAYMENT_ERROR, 'error_message' => __( 'Error', 'bookly' ) );
                    }
                } catch ( \Exception $e ) {
                    $response = array( 'success' => false, 'error' => Errors::PAYMENT_ERROR, 'error_message' => $e->getMessage() );
                }
            } else {
                $response = array(
                    'success'         => false,
                    'error'           => Errors::CART_ITEM_NOT_AVAILABLE,
                    'failed_cart_key' => $failed_cart_key,
                );
            }
        } else {
            $response = array( 'success' => false, 'error' => Errors::SESSION_ERROR );
        }

        // Output JSON response.
        wp_send_json( $response );
    }
    //ADWAVE END
}