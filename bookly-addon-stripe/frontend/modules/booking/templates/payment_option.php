<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
/** @var Bookly\Lib\CartInfo $cart_info */
use Bookly\Lib\Utils;
use Bookly\Lib as BooklyLib;
?>
<div class="bookly-box bookly-list">
    <label>
        <input type="radio" class="bookly-payment" name="payment-method-<?php echo $form_id ?>" value="card" data-form="stripe" />
        <span><?php echo Utils\Common::getTranslatedOption( 'bookly_l10n_label_pay_ccard' ) ?>
            <?php if ( $show_price ) : ?>
                <span class="bookly-js-pay"><?php echo Utils\Price::format( $cart_info->getPayNow() ) ?></span>
            <?php endif ?>
        </span>
        <img src="<?php echo $url_cards_image ?>" alt="cards" />
    </label>
    <!-- ADWAVE START -->
    <?php
    echo '<span class="bookly-none"><table class="mkd-payment-table-table">';
        echo '<thead>';
        echo '<tr>';
/*
        echo '<th>';
        echo 'Service';
    echo '</th>';
*/

// <!-- Adwave Start -->
    echo '<th>';
        echo 'Room';
    echo '</th>';
// <!-- Adwave End -->
    echo '<th>';
        echo 'Date';
    echo '</th>';
    echo '<th>';
        echo 'Time';
    echo '</th>';
    echo '<th>';
        echo 'Price';
    echo '</th>';
        echo '</tr>';
        echo '</thead>';
    echo '<tbody>';
    foreach ($cart_items as $key => $item) {
        echo '<tr>';
            $slots = $item->getSlots();
            $service_dp = BooklyLib\Slots\DatePoint::fromStr( $slots[0][2] )->toClientTz();
        /*
            echo '<td>';
            $service = $item->getService();
            if ($service && $service->getTitle()) {
                echo $service->getTitle() . '<br/>';
            }
            echo '</td>';
        */
            // <!-- Adwave Start -->
            echo '<td>';
                echo $item->getStaff()->getTranslatedName();
            echo '</td>';
            // <!-- Adwave End -->
            echo '<td>';
                echo $service_dp->formatI18nDate();
            echo '</td>';
            echo '<td>';
            // <!-- ADWAVE START -->
                if ( $item->getService()->getDuration() * $item->getUnits() < DAY_IN_SECONDS ) {
                    if($item->getUnits()>1)
                    echo $service_dp->formatI18nTime() . " ({$item->getUnits()} hours)";
                    else
                    echo $service_dp->formatI18nTime() . " ({$item->getUnits()} hour)";
            // <!-- ADWAVE END -->
                } else {
                    echo $item->getService()->getStartTimeInfo();
                }
            echo '</td>';
            echo '<td>';
                $price = BooklyLib\Utils\Price::format( $item->getServicePriceWithoutExtras() );
                $day_of_week = (int)date('N', strtotime( $slots[0][2] ));
                $is_competitive_day = false;
                $bookly_competitive_days = get_option('bookly_competitive_days', array());
                if (!$bookly_competitive_days) {
                    $bookly_competitive_days = array();
                } else {
                    $bookly_competitive_days = json_decode($bookly_competitive_days, true);
                }
                $book_date = strtotime($slots[0][2] );
                foreach ($bookly_competitive_days as $key => $date_range_obj) {
                    $from = strtotime($date_range_obj['from']);
                    $to = strtotime($date_range_obj['to']);
                    if ($book_date >= $from && $book_date <= $to) {
                        $is_competitive_day = true;
                    }
                }
                $is_week_day = ($day_of_week < 6) && ($day_of_week > 0);
                $duration = ($item->getService()->getDuration() * $item->getUnits()) / 3600;
                if ($duration >= 8 && $is_week_day && !$is_competitive_day) {
                    $price = $price . ' (30% OFF)';
                } else if ($duration >= 4 && $is_week_day && !$is_competitive_day) {
                    $price = $price . ' (20% OFF)';
                } else if ($duration >= 2 && $is_week_day && !$is_competitive_day) {
                    $price = $price . ' (10% OFF)';
                }
                // <!-- ADWAVE END -->

                if ( $item->toBePutOnWaitingList() ) {
                    $price = '(' . $price . ')';
                }
                echo $price;
            echo '</td>';
            echo '</tr>';
        }
        echo '<tr>';
            echo '<td colspan="3">';
                echo 'Subtotal';
            echo '</td>';
            echo '<td>';
                echo Utils\Price::format( $cart_info->getSubtotal() );
            echo '</td>';
        echo '</tr>';
        if ($cart_info->getCoupon()) {
        $coupon = $cart_info->getCoupon();
        echo '<tr>';
            echo '<td colspan="3">';
                // <!-- ADWAVE START -->
                echo 'Coupon Code :' . $coupon->getCode();
                // <!-- ADWAVE END -->
            echo '</td>';
            echo '<td>';
                if ($coupon->getDeduction() == 0) {
                    echo Utils\Price::format( $cart_info->getCouponDiscount() );
                } else {
                    echo Utils\Price::format( -1 * $coupon->getDeduction());
                }
            echo '</td>';
        echo '</tr>';
        }
        echo '<tr>';
            echo '<td colspan="3">';
                echo 'Tax';
            echo '</td>';
            echo '<td>';
                echo Utils\Price::format( $cart_info->getTotalTax());
            echo '</td>';
        echo '</tr>';
        echo '<tr>';
            echo '<td colspan="3">';
                echo 'Total';
            echo '</td>';
            echo '<td>';
                echo Utils\Price::format( $cart_info->getGatewayAmount() );
            echo '</td>';
        echo '</tr>';
    echo '</tbody>';
    echo '</table></span>';
    // <!-- ADWAVE START -->
    if ( get_option( 'bookly_stripe_publishable_key' ) != '' ) : ?>
        <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <?php endif ?>
    <form class="bookly-stripe" style="display: none; margin-top: 15px;">
        <input type="hidden" id="publishable_key" value="<?php echo get_option( 'bookly_stripe_publishable_key' ) ?>">
        <?php Bookly\Frontend\Components\Booking\CardPayment::render() ?>
    </form>
</div>