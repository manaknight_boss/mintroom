<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<div class="col-sm-3 col-lg-2">
    <div class="checkbox">
        <label>
            <input type="checkbox" id="bookly-show-step-cart" class="bookly-js-show-step" data-target="bookly-step-5" <?php checked( get_option( 'bookly_cart_enabled' ) ) ?>>
            <?php _e( 'Show Cart step', 'bookly' ) ?>
        </label>
    </div>
</div>